/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dynarray_from_str.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/24 16:33:56 by mbeilles          #+#    #+#             */
/*   Updated: 2019/11/27 20:48:14 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "dynarray.h"

t_dynarray				*ft_dynarray_from_str(
		const char * const str
)
{
	t_dynarray			*arr;

	arr = ft_dynarray_create(0, 0);
	ft_dynarray_push(arr, str, ft_strlen(str));
	return (arr);
}
