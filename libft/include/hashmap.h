/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hashmap.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/15 20:09:57 by mbeilles          #+#    #+#             */
/*   Updated: 2019/11/18 21:36:04 by njiall           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HASHMAP_H
# define HASHMAP_H

# include <stdint.h>
# include <stddef.h>
# include <stdbool.h>

# define HASHTABLE_SIZE	(1u << 16)

typedef uint16_t		t_hash_type;

typedef struct			s_hash_entry
{
	const uint8_t *			key;
	const void *			value;
	size_t			len;
	struct s_hash_entry		*next;

}						t_hash_entry;

typedef struct			s_hashmap
{
	t_hash_entry		*table[HASHTABLE_SIZE];
	uint64_t			length;
	t_hash_type			(*hash_function)(const void *, const size_t);
	size_t				(*size_evaluator)(const void *);
}						t_hashmap;

/*
** =============================================================================
** 					Hash functions
** =============================================================================
*/
typedef enum			e_hash_function_id
{
	HASH_FUNC_DJB2,
	HASH_FUNC_DJB2A,
	HASH_FUNC_FNV1A,
	// HASH_FUNC_MD4,
	HASH_FUNC_MAX, // Used for arrays
	HASH_FUNC_DEFAULT = HASH_FUNC_DJB2A,
}						t_hash_function_id;

t_hash_type				ft_hash_djb2(
		const void *obj,
		const size_t len
);
t_hash_type				ft_hash_djb2a(
		const void *obj,
		const size_t len
);
t_hash_type				ft_hash_fnv1a(
		const void *obj,
		const size_t len
);

/*
** =============================================================================
** 					Size evaluation functions
** =============================================================================
*/
size_t					ft_hash_strlen(
		const void *str
);

/*
** =============================================================================
** 					Creation
** =============================================================================
**
** 				Custom
*/
t_hashmap				*ft_hashmap_create_custom(
		t_hash_type (*hash_function)(const void *, const size_t),
		size_t (*size_evaluator)(const void *)
);
t_hashmap				ft_hashmap_create_custom_loc(
		t_hash_type (*hash_function)(const void *, const size_t),
		size_t (*size_evaluator)(const void *)
);
/*
** 				Wrapped creation
*/
t_hashmap				*ft_hashmap_create_id(
		t_hash_function_id,
		size_t (*size_evaluator)(const void *)
);
t_hashmap				ft_hashmap_create_id_loc(
		t_hash_function_id,
		size_t (*size_evaluator)(const void *)
);
/*
** Since most of the time we use string based hashing
*/
t_hashmap				*ft_hashmap_create_id_string(
		t_hash_function_id
);
t_hashmap				ft_hashmap_create_id_string_loc(
		t_hash_function_id
);
t_hashmap				*ft_hashmap_create_default(
		void
);
t_hashmap				ft_hashmap_create_default_loc(
		void
);
/*
** =============================================================================
** 					Destruction
** =============================================================================
*/
uint32_t				ft_hashmap_destroy(
		t_hashmap * const map
);
uint32_t				ft_hashmap_destroy_loc(
		t_hashmap * const map
);

/*
** =============================================================================
** 					Manipulation
** =============================================================================
*/
bool					ft_hashmap_get(
		t_hashmap * const map,
		const void *const key,
		const size_t len,
		const void ** const value
);
const void				*ft_hashmap_set(
		t_hashmap * const map,
		const void * const key,
		const size_t len,
		const void * const value
);
const void				*ft_hashmap_remove(
		t_hashmap * const map,
		const void * const key,
		const size_t len
);
t_hash_entry			*ft_hashmap_iterate(
		const t_hashmap * const map,
		uint32_t *table_iterator,
		uint32_t *bucket_iterator
);

/*
** =============================================================================
** 					Utilities
** =============================================================================
*/
void					ft_hashmap_print(
		t_hashmap *map
);

#endif
