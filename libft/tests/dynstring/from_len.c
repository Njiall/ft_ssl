#include <string.h>
#include <criterion/criterion.h>
#include <criterion/parameterized.h>
#include <criterion/logging.h>

#include "dynstring.h"

typedef struct s_test {
	char		str[4096];
	size_t		len;

}				t_test;

ParameterizedTestParameters(dynstring, from_len) {
	static t_test			tests[] = {
		(t_test){.str = "wow",	.len = 2},
		(t_test){.str = "lol",	.len = 1},
		(t_test){.str = "xd",	.len = 0},
		(t_test){.str = "",		.len = 0},
	};

	// generate parameter set
	return cr_make_param_array(t_test, tests, sizeof(tests) / sizeof(t_test));
}

ParameterizedTest(t_test *test, dynstring, from_len)
{
	t_dynstring	new = dynstring_from_len(test->str, test->len);
	size_t len = strlen(new);
	cr_expect(test->len == len,
			"Strings are not the same length: %zu != %zu", test->len, len
	);

	cr_expect(!strncmp(new, test->str, test->len),
			"Strings are not equal"
	);
}
