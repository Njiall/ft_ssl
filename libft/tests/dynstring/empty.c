#include <string.h>
#include <criterion/criterion.h>
#include <criterion/parameterized.h>
#include <criterion/logging.h>

#include "dynstring.h"

Test(dynstring, empty) {
	t_dynstring new = dynstring_empty();

	cr_expect(new[0] == '\0', "String not empty");
}
