#include <string.h>
#include <criterion/criterion.h>
#include <criterion/parameterized.h>
#include <criterion/logging.h>

#include "dynarray.h"
#include "ft_tests.h"

typedef struct	s_test {
	// Display
	char		desc[4096];

	// Outputs
	uint8_t		expect_array[4096];
	uint64_t	elen;

	// Variables
	uint8_t		data[4096];
	uint64_t	dlen;

	// Contents
	uint8_t		contents[4096];
	uint64_t	clen;
}				t_test;

ParameterizedTestParameters(dynarray, push) {
	static t_test			tests[] = {
		(t_test){
			.desc = "Push of 0x22.",

			.expect_array = 	{0x22},
			.data = 			{0x22},
			.elen = 1,
			.dlen = 1,
		},
		(t_test){
			.desc = "Push of 0x1010101010.",

			.expect_array = 	{[0 ... 4] = 0x10},
			.data = 			{[0 ... 4] = 0x10},
			.elen = 5,
			.dlen = 5,
		},
		(t_test){
			.desc = "Push of 0x1010101010 with contents of 0x55 on 128 bytes.",

			.expect_array = 	{[0 ... 127] = 0x55, [128 ... 132] = 0x10},
			.data = 			{[0 ... 4] = 0x10},
			.contents = 		{[0 ... 127] = 0x55},
			.elen = 256,
			.dlen = 128,

			.clen = 128,
		},
		(t_test){
			.desc = "Push of 0x1010101010 with contents of 0x55 on 1000 bytes.",

			.expect_array = 	{[0 ... 999] = 0x55, [1000 ... 1004] = 0x10},
			.data = 			{[0 ... 4] = 0x10},
			.contents = 		{[0 ... 999] = 0x55},
			.elen = 1128,
			.dlen = 128,
			.clen = 1000,
		},
	};

	// generate parameter set
	return cr_make_param_array(t_test, tests, sizeof(tests) / sizeof(t_test));
}

ParameterizedTest(t_test *test, dynarray, push)
{
	// Init
	t_dynarray		output = ft_dynarray_create_loc(0, 0);
	// Fills contents
	if (test->clen > 0)
		ft_dynarray_push(&output, test->contents, test->clen);

	// Run test
	ft_dynarray_push(&output, &test->data, test->dlen);

	// Gets results
	bool match_len = output.index == test->elen;
	bool match_content = !memcmp(
			output.array + output.offset,
			test->expect_array,
			test->elen
	);

	// Handle results
	cr_expect(match_len, "%s\nSize doesn't match, out: %llu != exp: %llu",
			test->desc,
			output.index, test->elen);
	cr_expect(match_content, "%s\nContent doesn't match:\n%s",
			test->desc,
			get_mem_diff(
				output.array + output.offset,
				test->expect_array,

				output.index,
				test->elen
			)
	);

	// Cleanup
	ft_dynarray_destroy(&output, false);
}
