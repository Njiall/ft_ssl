/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/08 15:49:16 by mbeilles          #+#    #+#             */
/*   Updated: 2020/01/10 20:04:12 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MD5_H
# define MD5_H

# include <stdint.h>
# include <stdlib.h>
# include <stdbool.h>
# include <sys/types.h>
# include <unistd.h>

# include "digest.h"

typedef union			u_md5_hash
{
	struct {
		uint8_t			hash[128 / 8];
	};
	struct {
		uint32_t		h0;
		uint32_t		h1;
		uint32_t		h2;
		uint32_t		h3;

		uint32_t		a;
		uint32_t		b;
		uint32_t		c;
		uint32_t		d;
		uint32_t		f;
		uint32_t		g;

		uint32_t		i;
	};
}						t_md5_hash;

typedef struct			s_md5_ctx
{
	uint8_t				data[512 / (sizeof(uint8_t) * 8)];
	t_md5_hash			md;

	uint64_t			bit_len;
	uint64_t			data_len;

	bool				last_block : 1;
	bool				insert_pad : 1;
}						t_md5_ctx;

/*
** Data preparation
*/
bool					ssl_md5_update_data(
		t_md5_ctx *ctx,
		const uint8_t *buff,
		ssize_t len
);
/*
** Data Proccessing
*/
void					ssl_md5_init(t_md5_ctx *ctx);
void					ssl_md5_vector(t_md5_ctx *ctx);
/*
** Data Render
*/

t_ssl_digest			ssl_md5_final_hex(t_md5_ctx *ctx);
t_ssl_digest			ssl_md5_final_bin(t_md5_ctx *ctx);

#endif
