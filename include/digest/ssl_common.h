/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ssl_common.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: njiall <mbeilles@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/30 12:02:29 by njiall            #+#    #+#             */
/*   Updated: 2019/12/31 13:45:43 by njiall           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SSL_COMMON_H
# define SSL_COMMON_H

# include <stddef.h>

typedef int				t_fd_type;

typedef struct			s_fd_list
{
	t_fd_type			*list;
	size_t				length;
}						t_fd_list;

typedef enum			e_ssl_input_type
{
	SSL_IN_NONE,
	SSL_IN_FD,
	SSL_IN_BUFFER,
	SSL_IN_MAX
}						t_ssl_input_type;

typedef union			u_ssl_input
{
	struct { // Default struct to get it's type with input.type
		t_fd_type			type;
	};

	struct s_ssl_input_fd// fd
	{
		t_ssl_input_type	type;
		t_fd_type			id;
	}
							fd;

	struct s_ssl_input_buffer// buffer
	{
		t_ssl_input_type	type;
		char				*buffer;
		size_t				length;
	}
							buffer;
}						t_ssl_input;

#endif
