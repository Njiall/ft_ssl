/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha2.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/11 09:06:46 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/13 18:52:55 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHA2_H
# define SHA2_H

# include <stdint.h>
# include <stdlib.h>
# include <stdbool.h>
# include <unistd.h>

# include "digest.h"

typedef union			u_sha2_hash
{
	struct {
		uint8_t			hash[512 / 8];
	};
	struct {
		uint32_t		state[512 / (8 * sizeof(uint32_t))];
		uint64_t		lstate[512 / (8 * sizeof(uint64_t))];
	};
}						t_sha2_hash;

typedef struct			s_sha2_ctx
{
	union {
		uint8_t			data[512 / 8];
		uint8_t			ldata[1024 / 8];
	};
	t_sha2_hash			md;

	union {
		uint64_t		bit_len;
		__uint128_t		lbit_len;
	};
	union {
		uint64_t		data_len;
		__uint128_t		ldata_len;
	};

	bool				last_block : 1;
	bool				insert_pad : 1;
}						t_sha2_ctx;

/*
** Data preparation
*/
bool					ssl_sha2_512_update_data(
		t_sha2_ctx *ctx,
		const uint8_t *buff,
		ssize_t len
);
bool					ssl_sha2_1024_update_data(
		t_sha2_ctx *ctx,
		const uint8_t *buff,
		ssize_t len
);
/*
** Data Proccessing
*/
void					ssl_sha2_224_init(t_sha2_ctx *ctx);
void					ssl_sha2_256_init(t_sha2_ctx *ctx);
void					ssl_sha2_384_init(t_sha2_ctx *ctx);
void					ssl_sha2_512_init(t_sha2_ctx *ctx);

void					ssl_sha2_256_vector(t_sha2_ctx *ctx);
void					ssl_sha2_512_vector(t_sha2_ctx *ctx);

t_ssl_digest			ssl_sha2_224_final_bin(t_sha2_ctx *ctx);
t_ssl_digest			ssl_sha2_256_final_bin(t_sha2_ctx *ctx);
t_ssl_digest			ssl_sha2_384_final_bin(t_sha2_ctx *ctx);
t_ssl_digest			ssl_sha2_512_final_bin(t_sha2_ctx *ctx);

#endif
