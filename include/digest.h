/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   digest.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/04 23:57:00 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/13 13:27:30 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DIGEST_H
# define DIGEST_H

# include <stdint.h>
# include <stdlib.h>
# include <stdbool.h>

typedef struct			s_ssl_digest
{
	uint8_t				*hash;
	size_t				length;
}						t_ssl_digest;

#endif
