#ifndef COMMAND_H
# define COMMAND_H

# include <stdlib.h>
# include <unistd.h>

# include "cipher.h"
# include "digest.h"

/*
** =============================================================================
** 					Command ids
** =============================================================================
**
** Here is a list of different modes of operation (Modus operandi)
** 		of block encription algorithms like DES:
** 			- CBC  Cipher Block Chaining
** 			- PCBC Propagating Cipher Block Chaining
** 			- CFB  Cipher FeedBack
** 			- OFB  Output FeedBack
** 			- ECB  Electronic CodeBook
** 			- CTR  CounTeR
**
** Sources:
**		- https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation
*/
typedef enum					e_ssl_cmd_id
{
	SSL_ID_NONE,
	SSL_ID_UTILS_START,
	// List all utility commands
	SSL_ID_HELP,
	SSL_ID_LIST,
	SSL_ID_CIPHER_LIST,
	SSL_ID_DIGEST_LIST,
	// End
	SSL_ID_UTILS_END,

	SSL_ID_CIPHER_START = SSL_ID_UTILS_END,
	// List all cipher commands here
	// Des family
	SSL_ID_DES_CBC,
	SSL_ID_DES_CFB,
	SSL_ID_DES_ECB,
	SSL_ID_DES_EDE,
	SSL_ID_DES_OFB,
	SSL_ID_DES_EDE_CBC,
	SSL_ID_DES_EDE_CFB,
	SSL_ID_DES_EDE_OFB,
	SSL_ID_DES_EDE3,
	SSL_ID_DES_EDE3_CBC,
	SSL_ID_DES_EDE3_CFB,
	SSL_ID_DES_EDE3_OFB,
	SSL_ID_DES3,
	SSL_ID_DESX,
	// Base 64
	SSL_ID_BASE_64,
	// Aes family
	SSL_ID_AES_128_CBC, // Bonus
	SSL_ID_AES_128_ECB, // Bonus
	SSL_ID_AES_192_CBC, // Bonus
	SSL_ID_AES_192_ECB, // Bonus
	SSL_ID_AES_256_CBC, // Bonus
	SSL_ID_AES_256_ECB, // Bonus
	// End
	SSL_ID_CIPHER_END,

	SSL_ID_DIGEST_START = SSL_ID_CIPHER_END,
	// List all digest commands here
	SSL_ID_MD5,
	SSL_ID_GOST, // Bonus
	// Sha-2 family
	SSL_ID_SHA2_224, // Bonus
	SSL_ID_SHA2_256,
	SSL_ID_SHA2_384, // Bonus
	SSL_ID_SHA2_512, // Bonus
	SSL_ID_SHA2_512_224, // Bonus
	SSL_ID_SHA2_512_256, // Bonus
	// Sha-3 family
	SSL_ID_SHA3_224, // Bonus
	SSL_ID_SHA3_256, // Bonus
	SSL_ID_SHA3_384, // Bonus
	SSL_ID_SHA3_512, // Bonus
	SSL_ID_SHAKE_128, // Bonus
	SSL_ID_SHAKE_256, // Bonus
	// End
	SSL_ID_DIGEST_END,
	SSL_ID_MAX
}							t_ssl_cmd_id;

/*
** =============================================================================
** 					Command types
** =============================================================================
*/
typedef enum				e_ssl_cmd_type
{
	SSL_TYPE_NONE,
	SSL_TYPE_CIPHER,
	SSL_TYPE_DIGEST,
	SSL_TYPE_SYSTEM,
	SSL_TYPE_UTILS,
	SSL_TYPE_MAX,
}							t_ssl_cmd_type;

typedef enum				e_ssl_default_opt_id
{
	SSL_OPT_NONE,
	// Global options
	SSL_OPT_DEBUG,
	SSL_OPT_GLOBAL_RANGE,
	// Digest default options
	SSL_OPT_DIG_ID_STR = SSL_OPT_GLOBAL_RANGE,
	SSL_OPT_DIG_ID_COL,
	SSL_OPT_DIG_ID_PRT,
	SSL_OPT_DIG_ID_REV,
	SSL_OPT_DIG_ID_SIL,
	SSL_OPT_DIG_ID_HEX,
	SSL_OPT_DIG_ID_COR,
	SSL_OPT_DIG_ID_BIN,
	SSL_OPT_DIG_ID_OUT,
	SSL_OPT_DIG_MAX,

	SSL_OPT_CPH_ID_HEX = SSL_OPT_GLOBAL_RANGE,
	SSL_OPT_CPH_ID_OUT,
	SSL_OPT_CPH_ID_MAX,

	SSL_OPT_SAFE_RANGE = 0x20,
	SSL_OPT_MAX = 64,
}							t_ssl_default_opt_id;

/*
** Use this on commands to name their custom options' ids.
*/
typedef enum				e_ssl_custom_opt_id
{
	SSL_OPT_B64_ENC = SSL_OPT_SAFE_RANGE,
	SSL_OPT_B64_DEC,
	SSL_OPT_B64_IN,
	SSL_OPT_B64_OUT,
	SSL_OPT_B64_MAX,
}								t_ssl_custom_opt_id;

/*
** =============================================================================
** 					Data types
** =============================================================================
*/

/*
** 				Paramter data
*/
typedef struct					s_ssl_cmd_arg
{
	// Tells how many times the option has been enabled.
	// This can be useful if you need to know if the usage has been correct.
	size_t						enabled;
	// This is a pointer to the option from the configuration.
	const char					*opt;

	// This is the array containing the args parsed from the cli.
	// If an option is called multiple times and takes at least a parameter
	// the arguments will get concatenated.
	// This can  come in handy when you want to have multiple outputs.
	// I.e.: `ft_ssl md5 -out file1 -out file2`
	const char					**args;
	// Length of the args array.
	size_t						length;
}								t_ssl_cmd_arg;

/*
** Data of the command typed with the cli.
** It contains any option an arguments that have been typed.
** If it gets to the handler it means that the command semanticaly is valid.
** You still need to check for context validity of the arguments.
*/
typedef struct					s_ssl_cmd_data
{
	const char					*cmd;
	// Id you can use as an access index to the command table.
	size_t						id;

	// The table of parsed options.
	t_ssl_cmd_arg				options[SSL_OPT_MAX];
	size_t						length;
}								t_ssl_cmd_data;

/*
** 				Structural data
*/
typedef struct					s_ssl_cmd_option
{
	// Data part
	const char					*aliases[4];
	size_t			 			length[4];
	const char					*desc;
	size_t			 			params;

	// Data description part.
	const char					*placeholder_names[4];
}								t_ssl_cmd_option;

# define CMD_DEFAULT \
	struct { \
		const t_ssl_cmd_type	type; \
		\
		const char * const		aliases[4]; \
		const size_t			length[4]; \
		const char * const		desc; \
		\
		const size_t			hash_length; \
		const char * const		hash_name; \
		const size_t			hash_name_length; \
		\
		const t_ssl_cmd_option	options[SSL_OPT_MAX - SSL_OPT_SAFE_RANGE]; \
		const size_t			count; \
		\
		const char * const		arguments_name; \
		const size_t			arguments_required; \
	}

typedef struct					s_ssl_cmd_utils
{
	CMD_DEFAULT;
	// Add things here
	void						(*function)(int argc, char **argv);
}								t_ssl_cmd_utils;

typedef struct					s_ssl_cmd_cipher
{
	CMD_DEFAULT;
	// Add things here
}								t_ssl_cmd_cipher;

typedef struct					s_ssl_cmd_digest
{
	CMD_DEFAULT;
	// Add things here
}								t_ssl_cmd_digest;

typedef struct					s_ssl_cmd_system
{
	CMD_DEFAULT;
	// Add things here
}								t_ssl_cmd_system;

// Tagged union description
typedef union					u_ssl_cmd
{
	CMD_DEFAULT;
	t_ssl_cmd_cipher			cipher;
	t_ssl_cmd_digest			digest;
	t_ssl_cmd_utils				utils;
}								t_ssl_cmd;

typedef enum					e_ssl_cmd_err_code
{
	SSL_ERR_NONE,
	SSL_ERR_INTERNAL_START, // Internal
	SSL_ERR_NO_HANDLER,
	SSL_ERR_INTERNAL_END, // Internal

	SSL_ERR_INPUT_START, // Input

	SSL_ERR_NO_COMMAND,
	SSL_ERR_WRONG_COMMAND,
	SSL_ERR_WRONG_OPTION,
	SSL_ERR_WRONG_OPTION_TYPE,
	SSL_ERR_WRONG_ARGUMENT,
	SSL_ERR_WRONG_ARGUMENT_TYPE,
	SSL_ERR_NOT_ENOUGH_ARGS,
	SSL_ERR_FILE_NOT_FOUND,
	SSL_ERR_HELP_INVALID_CMD,

	SSL_ERR_INPUT_END, // Input
	SSL_ERR_CRASH_START, // Crash
	SSL_ERR_FAILED_ALLOC,
	SSL_ERR_CRASH_END, // Crash
	SSL_ERR_MAX
}								t_ssl_cmd_err_code;

typedef struct					s_ssl_cmd_status
{
	// Error handle
	bool						valid;
	t_ssl_cmd_err_code			code;
	const char					*err[16];

	// Data
	t_ssl_cmd_id				cmd_id;
}								t_ssl_cmd_status;

typedef t_ssl_cmd_status		(t_ssl_cmd_handler)(
		const int,
		const char **,
		t_ssl_cmd_data
);
typedef t_ssl_cmd_status		(*t_ssl_cmd_hdl)(
		const int,
		const char **,
		t_ssl_cmd_data
);

/*
** =============================================================================
** 					Handlers
** =============================================================================
*/

typedef struct					s_ssl_cmd_ctx
{
	int							argc;
	const char					**argv;
	bool						alloc_argv;

	t_ssl_cmd_status			status;
	t_ssl_cmd_data				data;
	int							index;

	bool						halt;
}								t_ssl_cmd_ctx;

t_ssl_cmd_status				ssl_command_handle(
		const int argc,
		const char **argv
);

typedef t_ssl_cmd_status		(t_ssl_digest_handler)(
		t_ssl_cmd_data data,
		const char *path,
		int in,
		int out
);

/*
** =============================================================================
** 					Command lookup structure
** =============================================================================
*/

extern const t_ssl_cmd			g_ssl_cmds[SSL_ID_MAX];
extern const t_ssl_cmd_option	g_digest_default_options[SSL_OPT_SAFE_RANGE];
extern const t_ssl_cmd_option	g_cipher_default_options[SSL_OPT_SAFE_RANGE];
extern const t_ssl_cmd_option	*g_type_table[SSL_TYPE_MAX];
extern const t_ssl_cmd_hdl		g_handlers[SSL_ID_MAX];

/*
** =============================================================================
** 					Command hooks
** =============================================================================
*/


// MDX
t_ssl_cmd_handler				ssl_md4_digest;
t_ssl_digest_handler			ssl_md5_digest;
// SHA-2
t_ssl_digest_handler			ssl_sha2_224_digest;
t_ssl_digest_handler			ssl_sha2_256_digest;
t_ssl_digest_handler			ssl_sha2_384_digest;
t_ssl_digest_handler			ssl_sha2_512_digest;
t_ssl_cmd_handler				SSL_SHA2_512_224_digest;
t_ssl_cmd_handler				SSL_SHA2_512_256_digest;
// SHA-3
t_ssl_cmd_handler				SSL_SHA3_224_digest;
t_ssl_cmd_handler				SSL_SHA3_256_digest;
t_ssl_cmd_handler				SSL_SHA3_384_digest;
t_ssl_cmd_handler				SSL_SHA3_512_digest;
t_ssl_cmd_handler				SSL_SHAKE_128_digest;
t_ssl_cmd_handler				SSL_SHAKE_256_digest;
// Des
t_ssl_cmd_handler				ssl_des;
t_ssl_cmd_handler				ssl_des_cbc;
t_ssl_cmd_handler				ssl_des_cfb;
t_ssl_cmd_handler				ssl_des_ecb;
t_ssl_cmd_handler				ssl_des_ede;
t_ssl_cmd_handler				ssl_des_ede_cbc;
t_ssl_cmd_handler				ssl_des_ede_cfb;
t_ssl_cmd_handler				ssl_des_ede_ofb;
t_ssl_cmd_handler				ssl_des_ede3;
t_ssl_cmd_handler				ssl_des_ede3_cbc;
t_ssl_cmd_handler				ssl_des_ede3_cfb;
t_ssl_cmd_handler				ssl_des_ede3_ofb;
t_ssl_cmd_handler				ssl_des_ofb;
t_ssl_cmd_handler				ssl_des3;
t_ssl_cmd_handler				ssl_desx;
// Base 64
t_ssl_cmd_handler				ssl_base64;
// Aes
t_ssl_cmd_handler				ssl_aes_128_cbc_cipher;
t_ssl_cmd_handler				ssl_aes_128_ecb_cipher;
t_ssl_cmd_handler				ssl_aes_192_cbc_cipher;
t_ssl_cmd_handler				ssl_aes_192_ecb_cipher;
t_ssl_cmd_handler				ssl_aes_256_cbc_cipher;
t_ssl_cmd_handler				ssl_aes_256_ecb_cipher;
// Utils
t_ssl_cmd_handler				ssl_command_usage;
t_ssl_cmd_handler				ssl_command_list;
t_ssl_cmd_handler				ssl_command_list_digests;
t_ssl_cmd_handler				ssl_command_list_ciphers;
t_ssl_cmd_handler				ssl_command_list_encription;
t_ssl_cmd_handler				ssl_utils_digest_handler;

/*
** Debug
*/
t_ssl_cmd_status				ssl_command_print(
		const int argc,
		const char **argv,
		t_ssl_cmd_data data
);
void							ssl_command_status_print(
		t_ssl_cmd_status status
);
/*
** Utils
*/
// Returns if the `id` passed is a default option
bool							ssl_is_default_option(
		t_ssl_cmd_ctx *ctx,
		size_t id
);
// Returns if the `id` passed is a custom option
bool							ssl_is_custom_option(
		t_ssl_cmd_ctx *ctx,
		size_t id
);
// Cleans the global context from any allocated pointer
void							ssl_utils_command_cleanup(
		t_ssl_cmd_ctx *ctx
);
// Prints a usage for the command with `id` following the description of
// `src/config.c`
void							ssl_command_print_usage(
		t_ssl_cmd_id id,
		bool print_all
);
// Prints a general usage on the syntax of a command
void							ssl_command_print_general_usage(
		const char *command,
		const bool print_all
);
// Generates a formated buffer of a given hash to print
t_ssl_digest					ssl_genrate_digest(
		t_ssl_digest fingerprint,
		t_ssl_cmd_data data
);
// Extract the correct fd to output from the command data
int								get_output_fd(
		t_ssl_cmd_data data
);
// Prints the `output` of a `digest` given a `command data`
void							ssl_utils_print_digest(
		t_ssl_cmd_data data,
		t_ssl_digest md,
		int out,
		const char *path
);
// Gets the `id` of a command from a `name`. If it doesn't exist return
// `SSL_ID_NONE`
t_ssl_cmd_id					ssl_command_get_id(
		const char *name
);
// General purpose function that cuts a message of a given length and pads it.
// if the `in` fd is negative the function will read the `-id` of the option
// `SSL_OPT_DIG_ID_STR` or `--str`
//
// TODO Refactor to be able to use with every type of command.
bool							ssl_utils_message_proccess_input(
		t_ssl_cmd_data data,
		int in,
		void *ptr,
		size_t read_length,
		bool (*updater)(void*, const uint8_t *, ssize_t)
);
t_ssl_cmd_status				ssl_command_handle_interactive(void);
t_ssl_cmd_status				ssl_command_gen_data(
		const int argc,
		const char **argv,
		t_ssl_cmd_ctx *ctx
);
const t_ssl_cmd					*ssl_command_iterate(
		t_ssl_cmd_id *i
);
const t_ssl_cmd_option			*ssl_command_option_iterate(
		t_ssl_cmd_id id,
		size_t *i
);

#endif
