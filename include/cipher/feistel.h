#ifndef FEISTEL_H
#define FEISTEL_H

#include <stddef.h>
#include <stdint.h>

/*
** This is the function used to cypher rounds in the network.
** Returns the output message pointer passed.
*/
typedef void*		(*t_feistel_function)(
		void *, // Message
		void *, // Key
		void *  // Output message
);

typedef struct		s_feistel_network {

	t_feistel_function	f;

	size_t				left;
	size_t				right;

	// Key handling to pass to the round
	size_t				*key_schedule;
	void				**keys;

	
}					t_feistel_network;

#endif
