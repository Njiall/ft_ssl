#ifndef META_MACHINE_H
# define META_MACHINE_H

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

typedef enum	e_meta_class {
	MCL_None,
	MCL_End,
	MCL_Any,
	MCL_Max,
}				t_meta_class;

typedef enum	e_meta_state {
	MST_None,
	MST_Reset,
	MST_Reset_consume,
	MST_Init,
	MST_End,
	MST_Safe,
	MST_Max
}				t_meta_state;

#define				MM_Default(_input, _data) \
	(t_meta_machine){ \
		.prev = MST_Init, \
		.next = MST_Init, \
		.input = (const uint8_t *)_input, \
		.data = _data, \
	}

/*
 * The meta machine data structure should be unitilized with the macro
 * `MM_Default(input, data)` as a default to any meta machine you declare.
 */
typedef struct	s_meta_machine {
	// Internal data of the state machine.
	// This should not be modified by your hooks but by the transitions

	// Internal meta states stored in frames
	t_meta_state	prev;
	t_meta_state	next;
	// String input
	const void		*input;
	// Flags that are used internally
	struct {
		bool		debug: 1;
		FILE		*debug_fd;
	}				flags;

	// Data used by the hooks.
	// Can be anything: (A dirty example but you get the idea)
	// 		.data = &(struct {const uint8_t *anchor}){.anchor = input}
	void			*data;
}				t_meta_machine;

/*
** This is a `meta` state machine in the sense that it is adapted for any
** framework you may want to run.
** It totally can run anything you want as long as it's a regular language.
**
** Standard states:
**      - `MST_Init`: Initial state of the machine.
**                    It's useful order relative states.
**      - `MST_End`: Ends the machine
**      - `MST_Reset`: Resets the machine state to the init one without
**                     Consuming the input.
**      - `MST_Reset_consume`: Resets the state and consume the input.
**      - `MST_Safe`: Safe range of states to start your custom ones.
*/
static t_meta_machine		meta_machine_run(
		// State machine already initialized for your purpose.
		t_meta_machine m,
		// Limits to bound arrays.
		const size_t lclass,
		const size_t lstate,
		const size_t lhook,
		// Compress a char input into a char class, can serve as a filter.
		const t_meta_class (*consumor)(const void **),
		// Transition to dictate mode and state change.
		const t_meta_state transitions[lstate][lclass],
		// Hook table for handling special events.
		// You can use this to modify the 
		void (*const hooks[lstate][lstate][lhook])(t_meta_machine*)
)
{
	while (m.prev != MST_End && m.prev != MST_None && m.next != MST_End)
	{
		// Compute next frame for changing state machine state.
		m.next = transitions[m.prev][consumor(&m.input)];
		// Loops over hooks to call them.
		// Assumes that if one cell is empty then the rest is empty
		for (size_t hook = 0; hook < lhook && hooks[m.prev][m.next][hook]; ++hook)
			hooks[m.prev][m.next][hook](&m);

		// Consume input
		if (m.next != MST_Reset)
			m.input++;
		// Resets the state
		if (m.next == MST_Reset
				|| m.next == MST_Reset_consume)
			m.next = MST_Init;
		// Update states if we didn't end the machine
		if (m.next != MST_End && m.next != MST_None)
			m.prev = m.next;
	}

	return (m);
}

#endif
