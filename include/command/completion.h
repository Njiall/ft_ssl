#ifndef COMPLETION_H
#define COMPLETION_H

#include "meta_machine.h"
#include "linenoise.h"

typedef enum		e_ssl_cmd_class
{
	// Defaults
	CMDCL_None,
	CMDCL_End,
	// Wording
	CMDCL_Word,
	CMDCL_Space,
	CMDCL_Dash,
	// Litterals
	CMDCL_Lits,
	CMDCL_Litd,
	// Delimiter
	CMDCL_Max,
}					t_ssl_cmd_class;

#define CMDST_Any CMDST_None ... CMDST_Max - 1

typedef enum		e_ssl_cmd_state
{
	CMDST_None,
	// Defaults
	CMDST_Reset = MST_Reset,
	CMDST_Reset_consume = MST_Reset_consume,
	CMDST_Init = MST_Init,
	CMDST_End = MST_End,
	// Custom
	CMDST_Space = MST_Safe,
	CMDST_Cmd_name,
	CMDST_Cmd_arg,
	CMDST_Dash,
	CMDST_Lits,
	CMDST_Litd,
	// Delimiter
	CMDST_Max,
}					t_ssl_cmd_state;

const char						*ssl_command_complete(
		const char *input,
		linenoiseCompletions *lc
);

#define CMDHK_Max	2

#endif
