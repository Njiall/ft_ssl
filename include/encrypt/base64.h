#ifndef BASE64_H
# define BASE64_H

# include <stddef.h>
# include <stdint.h>

# define B64_BATCH		3

/*
** In the array sizes, 48 corresponds to the number to be red for one line
** 		of encoded output, 65 chars: 64 encode digits + 1 newline.
** And so you mutiply it by the batch of lines you want to proccess.
*/

typedef struct			s_base64_encode_ctx
{

	uint8_t				read_buf[B64_BATCH * 48];
	uint8_t				encode_buf[B64_BATCH * 65];

	size_t				read_length;
	size_t				encode_length;

}						t_base64_encode_ctx;

typedef struct			s_base64_decode_ctx
{

	uint8_t				encode_buf[B64_BATCH * 65];
	uint8_t				raw_buf[B64_BATCH * 48];

	size_t				encode_length;
	size_t				raw_length;

}						t_base64_decode_ctx;

#endif
