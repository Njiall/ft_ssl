/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: njiall <mbeilles@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/28 20:04:12 by njiall            #+#    #+#             */
/*   Updated: 2020/01/14 07:40:02 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DES_H
# define DES_H

# include <stdint.h>
# include <stdlib.h>
# include <stdbool.h>
# include <unistd.h>

// TODO Implement encrypt standard for ssl
//# include "encrypt.h"

typedef union			u_des_hash
{
	uint8_t				hash[512 / 8];
	uint32_t			state[512 / (8 * sizeof(uint32_t))];
}						t_des_hash;

typedef struct			s_des_ctx
{
	uint8_t				data[512 / 8];
	t_des_hash			md;

	uint64_t			bit_len;
	uint64_t			data_len;

	bool				last_block : 1;
	bool				insert_pad : 1;
}						t_des_ctx;

typedef struct			s_des_enc_ctx
{
	union {
		// Plain text data
		uint64_t		data;
		struct {
			uint32_t	data_lpt;
			uint32_t	data_rpt;
		};
	};
	uint64_t			key;
}						t_des_enc_ctx;

#endif
