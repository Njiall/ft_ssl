# FT SSL

### This project is realized for 42's ssl project.
It aims at reproducing basic features of **openssl**'s library of cryptographic functions and binary helper.
It's a pure `C` implementation of the functions below.

> This project is tested against `openssl 1.1.1d`, but should not use it in serious security applications.  
> To be clear this is done to the best of my ability but I'm doing this project alone so testing can be tedious :).
> To run the functional tests: `./test.sh <attempts> <command> ...`, so `./test.sh 1000 sha256 sha512`.

## Requirements

Requirements can be found on 42's CDN: (although it's purposely a bit cryptic)

- [ft_ssl_md5](https://cdn.intra.42.fr/pdf/pdf/5945/ft_ssl_md5.en.pdf)
- [ft_ssl_des](https://cdn.intra.42.fr/pdf/pdf/5941/ft_ssl_des.pdf)
- [ft_ssl_rsa](https://cdn.intra.42.fr/pdf/pdf/6894/en.subject.pdf)


## Cryptographic functions

- ### MD
	- [ ] v2
	- [ ] v4
	- [x] v5
	- [ ] v6

- ### SHA 2
	- [x] 224
	- [x] 256
	- [x] 384
	- [x] 512
	- [ ] 512/t
		- [ ] 512/224
		- [ ] 512/256

- ### SHA 3
	- [ ] 224
	- [ ] 256
	- [ ] 384
	- [ ] 512
	- [ ] Shake 128
	- [ ] Shake 256

- ### DES
	- [ ] des-cbc
	- [ ] des-cfb
	- [ ] des-ecb
	- [ ] des-ede
	- [ ] des-ede-cbc
	- [ ] des-ede-cfb
	- [ ] des-ede-ofb
	- [ ] des-ede3
	- [ ] des-ede3-cbc
	- [ ] des-ede3-cfb
	- [ ] des-ede3-ofb
	- [ ] des-ofb
	- [ ] des3
	- [ ] desx

- ### RSA
	- Todo

- ### AES
	- Todo
