# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    depend.mk                                          :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/08 18:36:26 by mbeilles          #+#    #+#              #
#    Updated: 2020/09/18 02:01:19 by mbeilles         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

-include ./strings.mk

#==============================================================================#
#                                 Flags detection                              #
#==============================================================================#

FAST_FLAG = -Ofast -march=native -flto
SLOW_FLAG = -fsanitize=address -g3 -O0

CFLAGS ?= -std=c18
FLAGS := $(CFLAGS)
ifeq	(,$(filter debug, $(MAKECMDGOALS)))
FLAGS += $(FAST_FLAG)
START_MSG = $(COMPILING_PRD)
else
FLAGS += $(SLOW_FLAG)
START_MSG = $(COMPILING_DBG)
endif

#==============================================================================#
#                                Path generation                               #
#==============================================================================#

PATH_COMPILE_DB?=./compile_commands.json

.PRECIOUS: $(PATH_OBJ)/. $(PATH_OBJ)%/.

$(PATH_OBJ)/.:
	@echo -ne $(CREATE_DIR) $@
	@mkdir -p $@

	# @printf "\r\033[K\033A"$(CREATE_SUBDIR) $(dir $(patsubst %/., %, $@)) $(notdir $(patsubst %/., %, $@))
$(PATH_OBJ)%/.:
	@mkdir -p $@

$(PATH_DEP)/.:
	@echo -ne $(CREATE_DIR) $@
	@mkdir -p $@

	# @printf "\r\e[K"$(CREATE_SUBDIR) $(dir $(patsubst %/., %, $@)) $(notdir $(patsubst %/., %, $@))
$(PATH_DEP)%/.:
	@mkdir -p $@

libraries = $(shell make -q -s -C $(1) || echo 'FORCE')

-include $(DEPS)
.PRECIOUS: $(DEPS)

#==============================================================================#
#                                 Default rules                                 #
#==============================================================================#

$(PATH_DEP)/%.d: ;

.SECONDEXPANSION:

FORCE:

all: $(NAME)

debug: $(NAME)

clean:
	@-rm -rf $(PATH_OBJ)
	@-rm -rf $(PATH_DEP)
	@-rm -rf $(PATH_COMPILE_DB)
	@printf $(CLEANING_OBJS)

fclean: clean
	@-rm -f $(NAME)
	@printf $(CLEANING_BINS)

re: fclean
	@$(MAKE) --no-print-directory all

#==============================================================================#
#                                 Dependencies                                 #
#==============================================================================#

ifeq ($(filter re,$(MAKECMDGOALS)),re)
MAKECMDGOALS:=$(filter-out re,$(MAKECMDGOALS)) fclean all
endif
ifeq ($(filter-out depend,$(MAKECMDGOALS)), )
MAKECMDGOALS+=all
depend:
	@for cmd in $(filter-out depend,$(MAKECMDGOALS)); do \
		for lib in $(foreach clib,$(CLIBS),$(dir $(clib))); do \
			printf $(MAKING_LIB) " $${lib%/} "; \
			$(MAKE) -C $$lib --no-print-directory depend $$cmd ; \
		done \
	done
	@printf $(MADE_LIB)
	@$(MAKE) -C .
else
depend:
	@for cmd in $(filter-out depend,$(MAKECMDGOALS)); do \
		for lib in $(foreach clib,$(CLIBS),$(dir $(clib))); do \
			printf $(MAKING_LIB) "$${lib%/}"; \
			$(MAKE) -C $$lib --no-print-directory depend $$cmd ; \
		done \
	done
	@printf $(MADE_LIB)
endif

#==============================================================================#
#                           C libraries compilation                            #
#==============================================================================#

ifneq ($(filter depend,$(MAKECMDGOALS)),depend)
ifneq ($(CLIBS), )
$(CLIBS): $$(strip $$(call libraries,$$(@D)))
	@printf $(MAKING_LIB) $(basename $(notdir $@))
	@$(MAKE) -C $(@D) --no-print-directory $(filter-out test, $(MAKECMDGOALS))
endif
endif

#==============================================================================#
#                             C files compilation                              #
#==============================================================================#

# $@ is %.o
# $< is %.c
$(PATH_OBJ)/%.o: %.c 					| $$(@D)/. $(PATH_DEP)/$$(*D)/. $(PATH_DEP)/compile/. $$(LDLIBS) $$(CLIBS)
$(PATH_OBJ)/%.o: %.c $(PATH_DEP)/%.d	| $$(@D)/. $(PATH_DEP)/$$(*D)/. $(PATH_DEP)/compile/. $$(LDLIBS) $$(CLIBS)
	@touch $(PATH_DEP)/compile/$(basename $(notdir $<)).o.json
	@$(CC) -MJ $(PATH_DEP)/compile/$(basename $(notdir $<)).o.json $(FLAGS) -MT $@ -MMD -MP -MF $(PATH_DEP)/$*.Td -c $< -o $@; \
	if [ "$$?" != "1" ]; then \
		printf $(COMPILING_OK) ; \
		exit 0; \
	else \
		printf $(COMPILING_KO); \
		exit 2; \
	fi
	@mv -f $(PATH_DEP)/$*.Td $(PATH_DEP)/$*.d &>/dev/null
	@touch $@ # Updating generated because time of modification can be out of sync

$(PATH_COMPILE_DB):
	sed -e '1s/^/[\n/' -e '$$s/,$$/\n]/' $(PATH_DEP)/compile/*.o.json > $(PATH_COMPILE_DB)

.PHONY: debug depend all fclean clean re FORCE
