# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    test_criterion.mk                                  :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/08 18:27:52 by mbeilles          #+#    #+#              #
#    Updated: 2020/09/21 02:13:42 by njiall           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #
-include ./strings.mk

# Ensures defaults so you don't have to.
PATH_TEST ?= tests
BIN_TEST ?= test

ifeq (NO_TEST_FILES,)
# If there is no tests to be compiled without specifing NO_TEST_FILES
# Then there is a problem and the test rule should be aborted.
$(call assert,$(TESTS),Test files are undefined, cannot run tests...)
else
# Gets all tests files for the conversion with the tests entry point
# Then gets all program sources excepts it's entry point
TEST_OBJS := $(patsubst %.c, $(PATH_OBJ)/%.o, $(TESTS)) \
	$(patsubst %.c, $(PATH_OBJ)/%.o, $(filter-out $(PROG_ENTRY_POINT),$(SRCS)))
endif

UNAME := $(shell uname -s)
ifeq ($(UNAME),Darwin)
# Handles user installed criterion with brew
CRIT_CFLAGS ?= -I$(HOME)/.brew/include
CRIT_LDFLAGS ?= -L$(HOME)/.brew/lib -lcriterion
endif
CRIT_LDFLAGS ?= -lcriterion

#FLAGS := $(CFLAGS) $(SLOW_FLAG) $(CRIT_CFLAGS) # Sets the debug flags for finding any underlying segfault.

test: $(TEST_OBJS) $(CLIBS)
	@printf $(COMPILING_TEST)
	@$(CC) $(LDFLAGS) $(LDLIBS) $(FLAGS) $(CRIT_LDFLAGS) -o $(BIN_TEST) $^ ; \
		if [ "$$?" != "1" ]; then \
			printf $(MAKING_TEST_SUCCESS); \
			exit 0; \
		else \
			printf $(MAKING_TEST_FAILURE); \
			exit 2; \
		fi
	@chmod +x test
	@printf $(TESTING)
	@./$(BIN_TEST) --list
	@./$(BIN_TEST) --full-stats -j0

.PHONY: test print
