# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/04 04:17:21 by mbeilles          #+#    #+#              #
#    Updated: 2020/09/23 03:50:50 by mbeilles         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#==============================================================================#
#                                  VARIABLES                                   #
#------------------------------------------------------------------------------#
#                                Customisation                                 #
#==============================================================================#

NAME = ft_ssl

NICK = SSL
PROJECT_COLOR = "\033[38;5;160m"
PROJECT_COLOR_ALT = "\033[38;5;124m"

CC = clang

#==============================================================================#
#                                   Sources                                    #
#==============================================================================#

SRCS =	\
		$(PATH_SRC)/main.c													\
		\
		$(PATH_SRC)/command/config.c										\
		$(PATH_SRC)/command/handler.c										\
		$(PATH_SRC)/command/interactive.c									\
		$(PATH_SRC)/command/linenoise.c										\
		\
		$(PATH_SRC)/command/completion/engine.c								\
		\
		$(PATH_SRC)/command/utils/command_cleanup.c							\
		$(PATH_SRC)/command/utils/option_type.c								\
		$(PATH_SRC)/command/utils/get_command_id.c							\
		$(PATH_SRC)/command/utils/get_output_fd.c							\
		$(PATH_SRC)/command/utils/command_iterate.c							\
		$(PATH_SRC)/command/utils/command_option_iterate.c					\
		\
		$(PATH_SRC)/command/utils/message_proccess_input.c					\
		$(PATH_SRC)/command/utils/generate_digest_string.c					\
		\
		$(PATH_SRC)/command/utils/print_command.c							\
		$(PATH_SRC)/command/utils/print_digest.c							\
		\
		$(PATH_SRC)/command/utils/default_digest_handler.c					\
		$(PATH_SRC)/command/commands/help.c									\
		$(PATH_SRC)/command/commands/list.c									\
		$(PATH_SRC)/command/commands/md5.c									\
		$(PATH_SRC)/command/commands/sha224.c								\
		$(PATH_SRC)/command/commands/sha256.c								\
		$(PATH_SRC)/command/commands/sha384.c								\
		$(PATH_SRC)/command/commands/sha512.c								\
		$(PATH_SRC)/command/commands/base64.c								\
		$(PATH_SRC)/command/commands/des.c									\
		\
		$(PATH_SRC)/command/usage.c											\
		\
		$(PATH_SRC)/crypto/md5/md5_init.c									\
		$(PATH_SRC)/crypto/md5/md5_vector.c									\
		$(PATH_SRC)/crypto/md5/md5_update.c									\
		$(PATH_SRC)/crypto/md5/md5_final_bin.c								\
		\
		$(PATH_SRC)/crypto/sha2/sha2_512_update.c							\
		$(PATH_SRC)/crypto/sha2/sha2_1024_update.c							\
		\
		$(PATH_SRC)/crypto/sha2/sha2_224_init.c								\
		$(PATH_SRC)/crypto/sha2/sha2_224_final_bin.c						\
		\
		$(PATH_SRC)/crypto/sha2/sha2_256_init.c								\
		$(PATH_SRC)/crypto/sha2/sha2_256_vector.c							\
		$(PATH_SRC)/crypto/sha2/sha2_256_final_bin.c						\
		\
		$(PATH_SRC)/crypto/sha2/sha2_384_init.c								\
		$(PATH_SRC)/crypto/sha2/sha2_384_final_bin.c						\
		\
		$(PATH_SRC)/crypto/sha2/sha2_512_init.c								\
		$(PATH_SRC)/crypto/sha2/sha2_512_vector.c							\
		$(PATH_SRC)/crypto/sha2/sha2_512_final_bin.c						\

INC = \
	   $(PATH_INC)			\
	   $(PATH_INC)/digest	\
	   $(PATH_INC)/encrypt	\

LIBS =	\
		./libft \

SYSLIBS =	 \

SYSLIBS_LINUX =	\

TESTS =	\

#==============================================================================#
#                                   Paths                                      #
#==============================================================================#

PATH_SRC := src
PATH_INC := include
PATH_OBJ := .obj
PATH_DEP := .dep

#==============================================================================#
#                                 Compilation                                  #
#==============================================================================#

LDLIBS = \

CLIBS = $(foreach dep, $(LIBS), $(dep)/$(notdir $(dep)).a) \

LDFLAGS =
LDFLAGS_DARWIN =
LDFLAGS_LINUX = -fuse-ld=lld # Don't forget to install lld as ld don't use .a files

CFLAGS = $(foreach inc, $(INC), -I$(inc)) \
		 $(foreach dep, $(LIBS), -I$(dep)/$(PATH_INC)) \
		 $(foreach dep, $(LDLIBS), -I$(dep)/$(PATH_INC)) \

#==============================================================================#
#                                   Various                                    #
#==============================================================================#

SHELL = bash

#==============================================================================#
#                             Variables Customizers                            #
#==============================================================================#

OBJS := $(patsubst %.c, $(PATH_OBJ)/%.o, $(SRCS))
DEPS := $(patsubst %.c, $(PATH_DEP)/%.d, $(SRCS))

#==============================================================================#
#                                    Rules                                     #
#==============================================================================#

include makefiles/bin.mk
include makefiles/test.mk
include makefiles/strings.mk
include makefiles/depend.mk
