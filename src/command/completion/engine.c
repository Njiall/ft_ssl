#include "command.h"
#include "command/meta_machine.h"
#include "command/completion.h"
#include "linenoise.h"

// Ignoring the enum conversion since we are just extending types
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wenum-conversion"

static const t_meta_class	g_cmd_compressor[256] = {
	['a' ... 'z'] = CMDCL_Word,
	['A' ... 'Z'] = CMDCL_Word,
	['0' ... '9'] = CMDCL_Word,
	['_'] = CMDCL_Word,
	['-'] = CMDCL_Dash,

	[' '] = CMDCL_Space,
	['\t'] = CMDCL_Space,
	['\r'] = CMDCL_Space,
	['\n'] = CMDCL_Space,
	['\f'] = CMDCL_Space,

	['\''] = CMDCL_Lits,
	['\"'] = CMDCL_Litd,
	['\0'] = CMDCL_End,
};

static const t_meta_state	g_cmd_transition[CMDST_Max][CMDCL_Max] = {
	// Ending
	[CMDST_Any][CMDCL_End] = CMDST_End,
	// Spacing
	[CMDST_Any][CMDCL_Space] = CMDST_Space,
	// First word is command name
	[CMDST_Init][CMDCL_Word] = CMDST_Cmd_name,
	[CMDST_Cmd_name][CMDCL_Word] = CMDST_Cmd_name,
	// Dash for options
	[CMDST_Cmd_name][CMDCL_Dash] = CMDST_Dash,
	[CMDST_Cmd_arg][CMDCL_Dash] = CMDST_Dash,
	[CMDST_Space][CMDCL_Dash] = CMDST_Dash,
	[CMDST_Dash][CMDCL_Dash] = CMDST_Dash,
	// Arguments
	[CMDST_Space][CMDCL_Word] = CMDST_Cmd_arg,
	[CMDST_Dash][CMDCL_Word] = CMDST_Cmd_arg,
	[CMDST_Cmd_arg][CMDCL_Word] = CMDST_Cmd_arg,
	// Literals
	[CMDST_Space][CMDCL_Lits] = CMDST_Lits,
	[CMDST_Lits][CMDCL_Lits] = CMDST_Lits,
	[CMDST_Space][CMDCL_Litd] = CMDST_Litd,
	[CMDST_Litd][CMDCL_Litd] = CMDST_Litd,
};

static void					hook(t_meta_machine *m)
{
	static char *states[CMDST_Max] = {
		[CMDST_Init] = "Init",
		[CMDST_Space] = "Space",
		[CMDST_Cmd_arg] = "Arg",
		[CMDST_Cmd_name] = "Cmd",
		[CMDST_Lits] = "'",
		[CMDST_Litd] = "\"",
		[CMDST_End] = "End",
		[CMDST_Dash] = "-",
		[CMDST_None] = "error",
	};

	/*dprintf(2, "state: %s -> %s\n", states[m->prev], states[m->next]);*/
}

typedef struct		s_compl_ctx
{
	t_ssl_cmd_id	id;
	char			*anchor;
	size_t			len;
}					t_compl_ctx;
static void					_in_word(t_meta_machine *m)
{
	t_compl_ctx		*ctx = (t_compl_ctx*)m->data;
	ctx->len++;
}
#include "libft.h"

static void					_get_cmd_id(t_meta_machine *m)
{
	t_compl_ctx		*ctx = (t_compl_ctx*)m->data;
	char *cmd = ft_strndup(ctx->anchor, (char*)m->input - ctx->anchor);
	ctx->id = ssl_command_get_id(cmd);
	free(cmd);
	/*dprintf(2, "id: '%.*s': %X\n", (int)(m->input - (void*)ctx->anchor), ctx->anchor, ctx->id);*/
}

static void					_drop_anchor(t_meta_machine *m)
{
	t_compl_ctx		*ctx = (t_compl_ctx*)m->data;
	ctx->anchor = (char*)m->input;
	ctx->len = 1;
	/*dprintf(2, "droped anchor: %p (%s)\n", ctx->anchor, ctx->anchor);*/
}

static void					(*const g_cmd_hook[CMDST_Max][CMDST_Max][CMDHK_Max])(
		t_meta_machine *
) = {
	[CMDST_Init][CMDST_Cmd_name] = {&hook, &_drop_anchor},
	[CMDST_Space][CMDST_Cmd_name] = {&hook, &_drop_anchor},
	[CMDST_Cmd_name][CMDST_Cmd_name] = {&_in_word},
	[CMDST_Cmd_name][CMDST_Space] = {&hook, &_get_cmd_id},
	[CMDST_Cmd_name][CMDST_End] = {&hook, &_get_cmd_id},

	[CMDST_Space][CMDST_Dash] = {&_drop_anchor},
	[CMDST_Dash][CMDST_Dash] = {&_in_word},
	[CMDST_Dash][CMDST_Cmd_arg] = {&_in_word},
	[CMDST_Cmd_arg][CMDST_Cmd_arg] = {&_in_word},

	[CMDST_Space][CMDST_Cmd_arg] = {&hook, &_drop_anchor},
	[CMDST_Cmd_arg][CMDST_Space] = {&hook},
	[CMDST_Cmd_arg][CMDST_End] = {&hook},

	[CMDST_Space][CMDST_Lits] = {&hook, &_drop_anchor},
	[CMDST_Lits][CMDST_Space] = {&hook},
	[CMDST_Space][CMDST_Litd] = {&hook, &_drop_anchor},
	[CMDST_Litd][CMDST_Space] = {&hook},
};
#pragma clang diagnostic pop

static const t_meta_class		consumor(
		const void **input
)
{
	t_meta_class		cl;
	char				*i;

	i = *((char**)input);
	cl = g_cmd_compressor[*i];
	++i;
	return (cl);
}
#include "libft.h"
#include "dynarray.h"

const char				*ssl_command_complete(
		const char *input,
		linenoiseCompletions *lc
)
{
	t_meta_machine	m;

	m = MM_Default(input,
			&((t_compl_ctx){
				.id = SSL_ID_NONE,
				.anchor = NULL,
			})
	);
	m = meta_machine_run(
		m, CMDCL_Max, CMDST_Max, CMDHK_Max,
		&consumor, g_cmd_transition, g_cmd_hook
	);
	t_compl_ctx ctx = *(t_compl_ctx*)m.data;
	if (ctx.id == SSL_ID_NONE) {
		// We complete a command here
		const t_ssl_cmd *cmd;
		t_ssl_cmd_id id = SSL_ID_NONE;
		while ((cmd = ssl_command_iterate(&id)))
			for (size_t idx = 0; idx < sizeof(cmd->aliases) / sizeof(*cmd->aliases) && cmd->aliases[idx]; ++idx)
				if (cmd->aliases[idx] == ft_strstr(cmd->aliases[idx], input))
					linenoiseAddCompletion(lc, cmd->aliases[idx]);
	}
	if (m.prev == CMDST_Cmd_arg || m.prev == CMDST_Dash) {
		// We complete an arg
		const t_ssl_cmd_option *opt;
		size_t i = SSL_OPT_NONE;
		while ((opt = ssl_command_option_iterate(ctx.id, &i))) {
			for (size_t idx = 0; idx < sizeof(opt->aliases) / sizeof(*opt->aliases) && opt->aliases[idx]; ++idx) {
				if (opt->aliases[idx] == ft_strnstr(opt->aliases[idx], ctx.anchor, ctx.len)) {
					t_dynarray cmd = ft_dynarray_create_loc(0, 0);
					ft_dynarray_push_str(&cmd, input);
					ft_dynarray_push_str(&cmd, opt->aliases[idx] + ((char*)m.input - ctx.anchor - 1));
					ft_dynarray_push(&cmd, "", 1);
					linenoiseAddCompletion(lc, (char*)cmd.array + cmd.offset);
					ft_dynarray_destroy(&cmd, false);
				}
			}
		}
	}
	fflush(stderr);
	return (NULL);
}
