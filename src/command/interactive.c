/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   interactive.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/18 00:11:15 by mbeilles          #+#    #+#             */
/*   Updated: 2020/09/23 02:20:04 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "command.h"
#include "linenoise.h"
#include "command/completion.h"
#include "libft.h"

#include <stdio.h>

static char				*_ssl_command_completion_hint(
		const char *buff,
		int *color,
		int *bold
)
{
	*color = 2;
	*bold = 0;
	if (ft_strlen(buff))
		for (t_ssl_cmd_id id = SSL_ID_NONE; id < SSL_ID_MAX; ++id) {
			const t_ssl_cmd cmd = g_ssl_cmds[id];
			for (size_t idx = 0; idx < sizeof(cmd.aliases) / sizeof(*cmd.aliases) && cmd.aliases[idx]; ++idx)
				if (cmd.aliases[idx] == ft_strstr(cmd.aliases[idx], buff))
					return ((char*)cmd.aliases[idx] + ft_strlen(buff));
		}
	return (NULL);
}

static void					_ssl_command_completion(
		const char *buff,
		linenoiseCompletions *lc
)
{
	ssl_command_complete(buff, lc);
}

static inline bool			_ssl_command_handle_error(
		t_ssl_cmd_ctx *ctx
)
{
	if (!ctx->status.valid)
		ssl_command_status_print(ctx->status);
	return (!ctx->status.valid);
}

/*static inline t_ssl_cmd_status	_ssl_command_run()*/

t_ssl_cmd_status			ssl_command_handle_interactive()
{
	t_ssl_cmd_ctx			ctx;
	char					*line;

	linenoiseSetCompletionCallback(&_ssl_command_completion);
	linenoiseSetHintsCallback(&_ssl_command_completion_hint);
	// Prints something so the user isn't lost
	/*ssl_command_print_general_usage("<command>", false);*/
	while ((line = linenoise("42SSL> "))) {
		// Handle command like normally here
		// Split the line into argc argv
		linenoiseHistoryAdd(line);
		const char **argv;
		int argc;
		if (!(argv = (const char **)ft_strvsplit(line, " \t\f\v\r\n")))
			continue;
		for (argc = 0; argv[argc]; argc++);

		// Special command to exit
		if (ft_strequ(argv[0], "quit") || ft_strequ(argv[0], "exit"))
			return ((t_ssl_cmd_status){.valid = true});

		ctx = (t_ssl_cmd_ctx){
			.argc = argc, .index = 1,
			.argv = argv, .alloc_argv = true,
			.status = (t_ssl_cmd_status){.valid = true},
			.data = (t_ssl_cmd_data){
				.cmd = argv[0],
				.id = SSL_ID_NONE,
			},
		};
		ssl_command_gen_data(argc, argv, &ctx);
		if (g_handlers[ctx.data.id])
			ctx.status = g_handlers[ctx.data.id](argc - ctx.index, argv + ctx.index, ctx.data);
		else if (ctx.data.id)
			ctx.status = (t_ssl_cmd_status){
					.code = SSL_ERR_NO_HANDLER,
					.err = {"Command not ", argv[0], " supported yet.\n"},
			};
		else if (_ssl_command_handle_error(&ctx))
			ssl_command_print_general_usage("command", false);
		ssl_utils_command_cleanup(&ctx);
	}
	return ((t_ssl_cmd_status){.valid = true});
}
