/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   usage.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/11 05:56:53 by mbeilles          #+#    #+#             */
/*   Updated: 2020/09/23 02:34:12 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

#include "command.h"
#include "libft.h"
#include "dynarray.h"

static inline size_t	_max_cmd_length(
		t_ssl_cmd_id start,
		t_ssl_cmd_id end
)
{
	size_t				len = 0;

	for (t_ssl_cmd_id id = start; id < end; ++id)
	{
		if (len < g_ssl_cmds[id].length[0] + 1)
			len = g_ssl_cmds[id].length[0] + 1;
	}
	return (len);
}

static inline size_t	_max_cmd_alias_length(
		t_ssl_cmd_id start,
		t_ssl_cmd_id end
)
{
	size_t				len = 0;

	for (t_ssl_cmd_id id = start; id < end; ++id)
	{
		t_ssl_cmd cmd = g_ssl_cmds[id];
		size_t			local = 0;
		for (size_t i = 0; i < sizeof(cmd.length) / sizeof(*cmd.length) && cmd.length[i] != 0; ++i)
			local += cmd.length[i] + 1;
		if (len < local)
			len = local;
	}
	return (len);
}

static inline size_t	_dash_length(
		t_ssl_cmd_option opt,
		size_t id
)
{
	size_t				len;
	for (len = 0; len < opt.length[id] && opt.aliases[id][len] == '-'; ++len);
	return (len);
}

static inline size_t	_placeholder_length(
		t_ssl_cmd_option opt
)
{
	size_t len = opt.params; // Spaces between placeholders + 1 before
	for (size_t j = 0; j < opt.params; ++j)
		// We add 2 for the formatting
		len += ft_strlen(opt.placeholder_names[j]) + 2;
	return (len);
}

static inline size_t	_max_opt_dash_length(
		t_ssl_cmd_id id
)
{
	size_t				len = 0;
	size_t				l;

	for (size_t i = 0; i < SSL_OPT_SAFE_RANGE; ++i) {
		t_ssl_cmd_option opt = g_type_table[g_ssl_cmds[id].type][i];
		for (size_t j = 0; j < sizeof(opt.aliases) / sizeof(*opt.aliases); ++j) {
			l = _dash_length(opt, j);
			if (len < l)
				len = l;
		}
	}
	for (size_t i = 0; i < g_ssl_cmds[id].count; ++i) {
		t_ssl_cmd_option opt = g_ssl_cmds[id].options[i];
		for (size_t j = 0; j < sizeof(opt.aliases) / sizeof(*opt.aliases); ++j) {
			l = _dash_length(opt, j);
			if (len < l)
				len = l;
		}
	}
	return (len);
}

static inline size_t	_max_opt_single_length(
		t_ssl_cmd_option opt,
		size_t id
)
{
	return (opt.length[id] - _dash_length(opt, id) + _placeholder_length(opt));
}

static inline size_t	_max_opt_length(
		t_ssl_cmd_option opt
)
{
	size_t				len = 0;

	for (size_t id = 0; id < sizeof(opt.length) / sizeof(*opt.length)
			&& opt.aliases[id]; ++id)
	{
		size_t l = _max_opt_single_length(opt, id);
		if (len < l)
			len = l;
	}
	return (len);
}

static inline size_t	_max_cmd_opt_length(
		t_ssl_cmd_id id,
		bool aliases
)
{
	size_t				len = 0;
	size_t				opt_len;

	if (aliases) {
		// Herited options
		for (int i = 0; i < SSL_OPT_SAFE_RANGE; ++i) {
			opt_len = _max_opt_length(g_type_table[g_ssl_cmds[id].type][i]);
			if (len < opt_len)
				len = opt_len;
		}
		// Assumes custom option are indexed at 0 with no gaps
		for (int i = 0; i < g_ssl_cmds[id].count; ++i) {
			opt_len = _max_opt_length(g_ssl_cmds[id].options[i]);
			if (len < opt_len)
				len = opt_len;
		}
	} else {
		// Herited options
		for (int i = 0; i < SSL_OPT_SAFE_RANGE; ++i) {
			opt_len = _max_opt_single_length(g_type_table[g_ssl_cmds[id].type][i], 0);
			if (len < opt_len)
				len = opt_len;
		}
		// Assumes custom option are indexed at 0 with no gaps
		for (int i = 0; i < g_ssl_cmds[id].count; ++i) {
			opt_len = _max_opt_single_length(g_ssl_cmds[id].options[i], 0);
			if (len < opt_len)
				len = opt_len;
		}
	}
	return (len);
}

static inline void		_print_cmd_range(
		char *header,
		t_ssl_cmd_id start,
		t_ssl_cmd_id end,
		bool print_aliases
)
{
	t_dynarray buffer = ft_dynarray_create_loc(0, 0);
	size_t printed;

	ft_dynarray_push_str(&buffer, header);
	size_t		pad = print_aliases
		? _max_cmd_alias_length(start, end)
		: _max_cmd_length(start, end);
	for (t_ssl_cmd_id id = start; id < end; ++id)
	{
		t_ssl_cmd cmd = g_ssl_cmds[id];
		if (!cmd.aliases[0]) continue;

		ft_dynarray_push(&buffer, "\e[0;1;34m", 9);
		ft_dynarray_push(&buffer, cmd.aliases[0], cmd.length[0]);
		ft_dynarray_push(&buffer, " ", 1);
		printed = cmd.length[0] + 1;

		if (print_aliases)
		for (size_t i = 1; cmd.aliases[i]; ++i)
		{
			ft_dynarray_push(&buffer, "\e[0;2;34m", 9);
			ft_dynarray_push(&buffer, cmd.aliases[i], cmd.length[i]);
			ft_dynarray_push(&buffer, " ", 1);
			printed += cmd.length[i] + 1;
		}

		if (g_ssl_cmds[id].desc) {
			for (size_t i = 0; printed + i < pad; ++i)
				ft_dynarray_push(&buffer, " ", 1);
			ft_dynarray_push(&buffer, "\e[0;2m-- \e[0m", 13);
			ft_dynarray_push_str(&buffer, cmd.desc);
		}
		ft_dynarray_push(&buffer, "\n", 1);
	}
	write(1, buffer.array + buffer.offset, buffer.index);
	ft_dynarray_destroy(&buffer, false);
}

void				ssl_command_print_general_usage(
		const char *command,
		const bool print_all
)
{
	printf("\e[2;37m[\e[0;31mSSL\e[2;37m]:\e[0m Usage: %s "
			"\e[2m[\e[0;1;32moptions\e[0;2m]\e[0m "
			"\e[2m[\e[1m--\e[0;2m]\e[0m "
			"\e[2m[\e[0;1;36marguments\e[0;2m]\e[0m\n",
			command);
	_print_cmd_range(
		"Supported command utilities:\n",
		SSL_ID_UTILS_START + 1,
		SSL_ID_UTILS_END,
		print_all
	);
	_print_cmd_range(
		"\nSupported command ciphers:\n",
		SSL_ID_CIPHER_START + 1,
		SSL_ID_CIPHER_END,
		print_all
	);
	_print_cmd_range(
		"\nSupported command utilities:\n",
		SSL_ID_DIGEST_START + 1,
		SSL_ID_DIGEST_END,
		print_all
	);
}

static inline void		_print_option(
		t_dynarray *buffer,
		const t_ssl_cmd_option opt,
		const size_t index,

		const size_t padding,
		const size_t dash_padding
)
{
	const char *const name = opt.aliases[index];
	const char *const desc = opt.desc;
	size_t printed = 0;
	size_t j;

	// Calculates dashes length
	for (j = 0; name[j] == '-'; ++j);
	// Add padding for dashes
	for (size_t l = 0; l < dash_padding - j + 1; ++l, ++printed)
		ft_dynarray_push(buffer, " ", 1);
	// Add dashes
	for (size_t l = 0; l < j; ++l, ++printed)
		ft_dynarray_push(buffer, "\e[0;32m-", 8);
	// Add option name
	if (index == 0)
		ft_dynarray_push_str(buffer, "\e[0;1;32m");
	else
		ft_dynarray_push_str(buffer, "\e[0;32m");
	ft_dynarray_push_str(buffer, name + j);
	printed += ft_strlen(name) - j - 1;
	// Add placeholders
	for (int l = 0; l < opt.params; ++l) {
		const char *p = opt.placeholder_names[l];
		ft_dynarray_push(buffer, " \e[0;2m<\e[0;35m", 15);
		ft_dynarray_push(buffer, p, ft_strlen(p));
		ft_dynarray_push(buffer, "\e[0;2m>\e[0m", 12);
		printed += ft_strlen(p) + 1 + 2;
	}
	// Add padding for the rest of option
	for (;printed < padding + dash_padding; ++printed)
		ft_dynarray_push(buffer, " ", 1);
	// Add the description if there is one
	if (desc && index == 0) {
		ft_dynarray_push(buffer, " \e[0;2m-- \e[0m", 14);
		ft_dynarray_push_str(buffer, desc);
	} else if (desc) {
		ft_dynarray_push(buffer, " \e[0;2m(\e[0;2;37malias\e[0;2m)\e[0m", 33);
	}
	// End the line
	ft_dynarray_push(buffer, "\n", 1);
	
}

void					ssl_command_print_usage(
		t_ssl_cmd_id id,
		bool print_aliases
)
{
	const char			*cmd;
	const char			*name;
	size_t				pad;
	size_t				dpad;
	size_t				i;
	size_t				j;


	// Handle command printing
	cmd = g_ssl_cmds[id].aliases[0];
	name = g_ssl_cmds[id].arguments_name ? g_ssl_cmds[id].arguments_name : "arguments";
	// Handle no command
	if (g_ssl_cmds[id].count == 0 && g_type_table[g_ssl_cmds[id].type] == NULL)
	{

		printf("\e[2;37m[\e[0;31mSSL\e[2;37m]:\e[0m Usage: ft_ssl "
				"\e[1;34m%s\e[0m "
				"\e[2m[\e[0;1;36m%s\e[0;2m]\e[0m\n",
				cmd, name);
		return ;
	}

	if (g_ssl_cmds[id].desc)
	// Prints the usage of the command
		printf("\e[2;37m[\e[0;31mSSL\e[2;37m]:\e[0m Usage: ft_ssl "
				"\e[1;34m%s\e[0m "
				"\e[2m[\e[0;1;32moptions\e[0;2m]\e[0m "
				"\e[2m[\e[1m--\e[0;2m]\e[0m "
				"\e[2m[\e[0;1;36m%s\e[0;2m]\e[0m\n%s\n",
				cmd, name, g_ssl_cmds[id].desc);
	else
		printf("\e[2;37m[\e[0;31mSSL\e[2;37m]:\e[0m Usage: ft_ssl "
				"\e[1;34m%s\e[0m "
				"\e[2m[\e[0;1;32moptions\e[0;2m]\e[0m "
				"\e[2m[\e[1m--\e[0;2m]\e[0m "
				"\e[2m[\e[0;1;36m%s\e[0;2m]\e[0m\n",
				cmd, name);

	pad = _max_cmd_opt_length(id, print_aliases);
	dpad = _max_opt_dash_length(id);
	// Alloc some dynarray to print
	t_dynarray buffer = ft_dynarray_create_loc(0, 0);
	// Loops over option of the command
	// Prints options
	const t_ssl_cmd_option *opt;
	i = 0;
	while ((opt = ssl_command_option_iterate(id, &i)))
	{
		_print_option(&buffer, *opt, 0, pad, dpad);
		if (print_aliases)
			for (j = 1; j < sizeof(opt->aliases) / sizeof(*opt->aliases) && opt->aliases[j]; ++j)
				_print_option(&buffer, *opt, j, pad, dpad);
	}
	write(1, buffer.array + buffer.offset, buffer.index);
	ft_dynarray_destroy(&buffer, false);
}
