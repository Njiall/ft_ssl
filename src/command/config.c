/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   config.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: njiall <mbeilles@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/29 22:28:51 by njiall            #+#    #+#             */
/*   Updated: 2020/09/23 05:56:52 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "command.h"
#pragma clang diagnostic push
// Removes warnings for reinitilation of data
#pragma clang diagnostic ignored "-Winitializer-overrides"

/*
** Handlers used for every command
** TODO integrate inside the command list data structure: `g_ssl_cmds`
*/

const t_ssl_cmd_hdl			g_handlers[SSL_ID_MAX] = {
	// Utils
	[SSL_ID_HELP] = 	&ssl_command_usage,
	[SSL_ID_LIST] = 	&ssl_command_list,
	[SSL_ID_BASE_64] = 	&ssl_base64,
	// Digests
	[SSL_ID_MD5] = 		&ssl_utils_digest_handler,
	[SSL_ID_SHA2_224 ... SSL_ID_SHA2_512] = &ssl_utils_digest_handler,
};

const t_ssl_cmd_option		g_default_options[SSL_OPT_SAFE_RANGE] = {
	[SSL_OPT_DEBUG] = 		(t_ssl_cmd_option){.aliases = {"--debug"}, 	.length = {7}, 	.desc = "Prints debug information"},
};

const t_ssl_cmd_option		g_digest_default_options[SSL_OPT_SAFE_RANGE] = {
	[SSL_OPT_DEBUG] = 		(t_ssl_cmd_option){.aliases = {"--debug"}, 				.length = {7}, 		.desc = "Prints debug information"},
	[SSL_OPT_DIG_ID_HEX] = 	(t_ssl_cmd_option){.aliases = {"--hex", "-h"}, 			.length = {5, 2}, 	.desc = "Outputs as hexadecimal"},
	[SSL_OPT_DIG_ID_BIN] = 	(t_ssl_cmd_option){.aliases = {"--binary", "-b"}, 		.length = {8, 2}, 	.desc = "Outputs as binary"},
	[SSL_OPT_DIG_ID_COL] = 	(t_ssl_cmd_option){.aliases = {"--cols", "-c"}, 		.length = {5, 2}, 	.desc = "Outputs with separating columns"},
	[SSL_OPT_DIG_ID_SIL] = 	(t_ssl_cmd_option){.aliases = {"--quiet", "-q"}, 		.length = {7, 2}, 	.desc = "Toggle quiet mode (Strips unnecessary data)"},
	[SSL_OPT_DIG_ID_COR] = 	(t_ssl_cmd_option){.aliases = {"--coreutils", "-r"}, 	.length = {11, 2}, 	.desc = "Outputs with coreutils format"},
	[SSL_OPT_DIG_ID_PRT] = 	(t_ssl_cmd_option){.aliases = {"-p"}, 					.length = {2}, 		.desc = "Prints stdin and append sum"},
	[SSL_OPT_DIG_ID_OUT] = 	(t_ssl_cmd_option){
		.aliases = {"--out", "-o"}, 	.length = {5, 2}, .params = 1,
		.desc = "Outputs to <file> instead of stdout",
		.placeholder_names = {"file"},
	},
	[SSL_OPT_DIG_ID_STR] = 	(t_ssl_cmd_option){
		.aliases = {"--str", "-s"}, 	.length = {5, 2}, .params = 1,
		.desc = "Inputs a <string> instead of a file",
		.placeholder_names = {"string"},
	},
};

const t_ssl_cmd_option		g_cipher_default_options[SSL_OPT_SAFE_RANGE] = {
	[SSL_OPT_DEBUG] = 		(t_ssl_cmd_option){.aliases = {"--debug"}, 	.length = {7}, 	.desc = "Prints debug information"},
};

const t_ssl_cmd_option		*(g_type_table[SSL_TYPE_MAX]) = {
	[SSL_TYPE_NONE ... SSL_TYPE_MAX - 1] = g_default_options,
	[SSL_TYPE_DIGEST] = g_digest_default_options,
	[SSL_TYPE_CIPHER] = g_cipher_default_options,
};

const t_ssl_cmd				g_ssl_cmds[SSL_ID_MAX] = {
	// Utils
	[SSL_ID_HELP] = (t_ssl_cmd){
		.type = SSL_TYPE_UTILS,
		.aliases = {"help"},
		.length = {4},
		.desc = "Prints this :)",
		.arguments_name = "command name",
		.options = {
			[0] = (t_ssl_cmd_option){.aliases = {"--all", "-a"}, 	.length = {5, 2}, .desc = "Display all information available"},
		},
		.count = 1,
	},
	[SSL_ID_LIST] = (t_ssl_cmd){
		.type = SSL_TYPE_UTILS,
		.aliases = {"list"},
		.length = {4},
		.desc = "Lists command for a given category",
		.arguments_name = "category",
		.options = {
			[0] = (t_ssl_cmd_option){.aliases = {"ciphers"}, .length = {7}, .desc = "Lists all cipher commands"},
			[1] = (t_ssl_cmd_option){.aliases = {"disgests"}, .length = {8}, .desc = "Lists all digests commands"},
			[2] = (t_ssl_cmd_option){.aliases = {"encrypt"}, .length = {7}, .desc = "Lists all encription commands"},
		},
		.count = 3,
	},
	[SSL_ID_CIPHER_LIST] = (t_ssl_cmd){
		.type = SSL_TYPE_UTILS,
		.aliases = {"ciphers"},
		.length = {7},
		.desc = "Lists all available ciphers"
	},
	[SSL_ID_DIGEST_LIST] = (t_ssl_cmd){
		.type = SSL_TYPE_UTILS,
		.aliases = {"digests"},
		.length = {7},
		.desc = "Lists all available digests"
	},
	// Digests
	[SSL_ID_SHA2_512] = (t_ssl_cmd){
		.type = SSL_TYPE_DIGEST,
		.aliases = {"sha512"},
		.length = {6},
		.hash_name = "SHA512",
		.hash_name_length = 6,
		.desc = "Digest of type sha2, it's hash value is on 512 bits",
		.hash_length = 512 / 8,
	},
	[SSL_ID_SHA2_384] = (t_ssl_cmd){
		.type = SSL_TYPE_DIGEST,
		.aliases = {"sha384"},
		.length = {6},
		.hash_name = "SHA384",
		.hash_name_length = 6,
		.desc = "Digest of type sha2, it's hash value is on 384 bits",
		.hash_length = 384 / 8,
	},
	[SSL_ID_SHA2_256] = (t_ssl_cmd){
		.type = SSL_TYPE_DIGEST,
		.aliases = {"sha256", "sha"},
		.length = {6, 3},
		.hash_name = "SHA256",
		.hash_name_length = 6,
		.desc = "Digest of type sha2, it's hash value is on 256 bits",
		.hash_length = 256 / 8,
	},
	[SSL_ID_SHA2_224] = (t_ssl_cmd){
		.type = SSL_TYPE_DIGEST,
		.aliases = {"sha224"},
		.length = {6},
		.hash_name = "SHA224",
		.hash_name_length = 6,
		.desc = "Digest of type sha2, it's hash value is on 224 bits",
		.hash_length = 224 / 8,
	},
	[SSL_ID_MD5] = (t_ssl_cmd){
		.type = SSL_TYPE_DIGEST,
		.aliases = {"md5"},
		.length = {3},
		.hash_name = "MD5",
		.hash_name_length = 3,
		.desc = "Digest of type mdx,  it's hash value is on 128 bits",
		.hash_length = 128 / 8,
	},
	// Ciphers
	[SSL_ID_DES_CBC] = (t_ssl_cmd){
		.type = SSL_TYPE_CIPHER,
		.aliases = {"des-cbc", "des"},
		.length = {7, 3},
	},
	[SSL_ID_DES_CFB] = (t_ssl_cmd){
		.type = SSL_TYPE_CIPHER,
		.aliases = {"des-cfb"},
		.length = {7},
	},
	[SSL_ID_DES_ECB] = (t_ssl_cmd){
		.type = SSL_TYPE_CIPHER,
		.aliases = {"des-ecb"},
		.length = {7},
	},
	[SSL_ID_DES_EDE3] = (t_ssl_cmd){
		.type = SSL_TYPE_CIPHER,
		.aliases = {"des-ede3", "des3"},
		.length = {8, 4},
		.desc = "Ecription function",
	},
	[SSL_ID_BASE_64] = (t_ssl_cmd){
		.type = SSL_TYPE_CIPHER,
		.aliases = {"base64"},
		.length = {6},
		.arguments_name = "file",
		.desc = "Encode and decode base64 data",
		.options = {
			[SSL_OPT_B64_ENC - 	SSL_OPT_SAFE_RANGE] = (t_ssl_cmd_option){.aliases = {"-e"}, 	.length = {2}, .desc = "Set encription mode (default)"},
			[SSL_OPT_B64_DEC - 	SSL_OPT_SAFE_RANGE] = (t_ssl_cmd_option){.aliases = {"-d"}, 	.length = {2}, .desc = "Set decription mode"},
			[SSL_OPT_B64_OUT - 	SSL_OPT_SAFE_RANGE] = (t_ssl_cmd_option){
				.aliases = {"-o", "--out"}, 	.length = {2, 5}, .desc = "Output to <file>", 	.params = 1,
				.placeholder_names = {"file"},
			},
			[SSL_OPT_B64_IN - 	SSL_OPT_SAFE_RANGE] = (t_ssl_cmd_option){
				.aliases = {"-i", "--in"}, 		.length = {2, 4}, .desc = "Input from <file>", 	.params = 1,
				.placeholder_names = {"file"},
			},
		},
		.count = 4,
	},
};

#pragma clang diagnostic pop
