/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handler.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/08 17:56:06 by mbeilles          #+#    #+#             */
/*   Updated: 2020/09/18 02:46:49 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "command.h"
#include "digest.h"
#include "libft.h"

#define COMMAND_PLACEHOLDER "\e[2m<\e[0;1;34mcommand\e[0;2m>\e[0m"

static inline t_ssl_cmd_status	ssl_assign_option_arg(
		t_ssl_cmd_ctx *ctx,
		t_ssl_cmd_option opt,
		int index
)
{
	// Enables option selected
	// Enabled is coded on a size_t meaning 64bits nowadays
	// but it's possible it would overflow
	// if this is being used with too many arguments
	ctx->data.options[index].enabled++;
	ctx->data.options[index].opt = ctx->argv[ctx->index];

	// If there isn't any argument to get
	if (!opt.params)
		return (ctx->status);

	// If any args are required and not already allocated, allocates them
	if (opt.params
			&& !(ctx->data.options[index].args = realloc(
					// If not allocated, then points to NULL
					ctx->data.options[index].args,
					// Adds already stored options to new ones
					(ctx->data.options[index].length + opt.params)
					* sizeof(const char *))))
		ft_exit(SSL_ERR_FAILED_ALLOC, "No space left on device.\n", NULL);

	int							i;
	// Loops over arguments to store strings as the option's arguments
	for (i = 0; i < opt.params && i + ctx->index + 1 < ctx->argc; ++i)
		ctx->data.options[index].args[
			ctx->data.options[index].length + i
		] = ctx->argv[ctx->index + i + 1];

	// If there wasn't enough arguments to option, throws an error
	if (i != opt.params)
		return ((t_ssl_cmd_status){
				.code = SSL_ERR_NOT_ENOUGH_ARGS,
				.err = {
					"Not enough arguments were provided to option '",
					ctx->argv[ctx->index],
					"' of '", g_ssl_cmds[ctx->data.id].aliases[0], "'.\n"
				},
		});

	// Offsets the index over args to what has been consumed
	ctx->index += i;
	// Writes the number of arguments to the option
	ctx->data.options[index].length += i;
	return (ctx->status);
}

/*
** Returns true when a break need to occur
*/

static inline bool				ssl_command_handle_option(
		t_ssl_cmd_ctx *ctx,
		int i
)
{
	t_ssl_cmd_option			opt;

	// If the arg is a double dash we should stop option reading
	if (ft_strlen(ctx->argv[ctx->index]) == 2
			&& ft_strnequ(ctx->argv[ctx->index], "--", 2)
			&& (ctx->halt = true))
		return (true);
	// Check for custom options for command, can override default options
	else if (ssl_is_custom_option(ctx, i))
	{
		opt = g_ssl_cmds[ctx->data.id].options[i - SSL_OPT_SAFE_RANGE];
		// We create it and gather its arguments
		ctx->status = ssl_assign_option_arg(ctx, opt, i);
		return (ctx->status.valid);
	}
	// Check for default options for command type options
	else if (ssl_is_default_option(ctx, i))
	{
		// Gets option from table
		opt = g_type_table[g_ssl_cmds[ctx->data.id].type][i];
		ctx->status = ssl_assign_option_arg(ctx, opt, i);
		return (ctx->status.valid);
	}
	return (false);
}

static inline t_ssl_cmd_status	ssl_command_handle_options(
		t_ssl_cmd_ctx *ctx
)
{
	int					i;

	// Loops over arguements of the command
	while (ctx->index < ctx->argc && !ctx->halt)
	{
		// Find the option of the argument refers to
		i = -1;
		while (++i < SSL_OPT_SAFE_RANGE + g_ssl_cmds[ctx->data.id].count)
			if (ssl_command_handle_option(ctx, i))
				break ;

		// Determinate if invalid option or start of arguments
		if (ctx->status.valid // There is already an error so we need to break
				&& i >= g_ssl_cmds[ctx->data.id].count + SSL_OPT_SAFE_RANGE)
			if (ctx->argv[ctx->index][0] == '-') // Invalid option
				return ((t_ssl_cmd_status){
						.code = SSL_ERR_WRONG_OPTION,
						.err = {
							"Option '",
							ctx->argv[ctx->index],
							"' doesn't exist for command ",
							ctx->argv[0],
							".\n",
						}
				});
			else // Got an argument so we need to break out of there
				break ;
		else
			++ctx->index;
	}

	return (ctx->status);
}

/*
** Generates and Handles a command from argc and argv.
**
** Command agnostic syntax:
**
** <cmd_name> [<options>] [--] [<arguments>]
**
** PS:	We will heavily use compound literals here as they are extremely useful
** 			to data sanitation, since not initialized fields are set to `0`.
** 		As well, it's okay to put string literals as contrary to the compound,
** 			they are not on the stack but the BSS since it's only a pointer.
** 		So we will never return a pointer of compound literal, but will possibly
** 			send an annonimised one into a function with this syntax:
** 			foo(&(my_struct){.bar = 2});
*/

t_ssl_cmd_status				ssl_command_gen_data(
		const int argc,
		const char **argv,
		t_ssl_cmd_ctx *ctx
)
{
	t_ssl_cmd_status			handle;

	ctx->data.id = ssl_command_get_id(ctx->argv[ctx->index - 1]);
	// Handle invalid command
	if (ctx->data.id <= SSL_ID_NONE)
		return ((t_ssl_cmd_status){
				.code = SSL_ERR_WRONG_COMMAND,
				.err = {"Command '", ctx->argv[ctx->index - 1], "' does not exist.\n"},
		});
	ctx->data.length = g_ssl_cmds[ctx->data.id].count;

	// Launch option finder
	if (!(handle = ssl_command_handle_options(ctx)).valid)
	{
		// Prints command information feedback to user
		ssl_command_status_print(handle);
		ssl_command_print_usage(ctx->data.id, false);
		// Cleanup before leaving
		ssl_utils_command_cleanup(ctx);
		return ((t_ssl_cmd_status){.code = handle.code});
	}

	// Debug printing of the command data structure
	if (ctx->data.options[SSL_OPT_DEBUG].enabled)
		ssl_command_print(argc - ctx->index, argv + ctx->index, ctx->data);
	return ((t_ssl_cmd_status){.valid = true});
}

t_ssl_cmd_status				ssl_command_handle(
		const int argc,
		const char **argv
)
{
	t_ssl_cmd_ctx				ctx;

	// Handle no command. Launch interactive mode
	if (argc < 2)
		return (ssl_command_handle_interactive());
	// Init command context with offsets to parse the rest of the options
	// 		and data with command parsed
	ctx = (t_ssl_cmd_ctx){
			.argc = argc, .index = 2,
			.argv = argv,
			.status = (t_ssl_cmd_status){.valid = true},
			.data = (t_ssl_cmd_data){
					.cmd = argv[1],
					.id = SSL_ID_NONE,
			},
	};
	ssl_command_gen_data(argc, argv, &ctx);
	// Run command handler with filled ctx
	if (g_handlers[ctx.data.id])
	{
		ctx.status = g_handlers[ctx.data.id](argc - ctx.index, argv + ctx.index, ctx.data);
		// Cleanup before leaving
		ssl_utils_command_cleanup(&ctx);
		return (ctx.status);
	} // No handler found, return error
	// Cleanup before leaving
	ssl_utils_command_cleanup(&ctx);
	return ((t_ssl_cmd_status){
			.code = SSL_ERR_NO_HANDLER,
			.err = {"Command ", argv[1], " not supported yet.\n"},
	});
}
