/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_command_id.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: njiall <mbeilles@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/28 20:38:18 by njiall            #+#    #+#             */
/*   Updated: 2020/09/23 04:06:48 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "command.h"
#include "libft.h"

t_ssl_cmd_id					ssl_command_get_id(
		const char *cmd
)
{
	size_t						len;
	t_ssl_cmd_id				id;

	// Precalculate length since we are going to use it a lot
	// Since we are looping on all ids
	len = ft_strlen(cmd);
	if (len < 0)
		return (SSL_ID_NONE);

	// Loops on all command ids
	id = SSL_ID_NONE;
	while (id < SSL_ID_MAX)
	{
		for (int i = 0; i < sizeof(g_ssl_cmds[id].aliases) / sizeof(char*); ++i)
			if (len == g_ssl_cmds[id].length[i]
					&& ft_strnequ(cmd, g_ssl_cmds[id].aliases[i], g_ssl_cmds[id].length[i]))
				return (id);
		++id;
	}
	return (SSL_ID_NONE);
}
