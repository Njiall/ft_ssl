/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   message_proccess_input.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/13 18:05:36 by mbeilles          #+#    #+#             */
/*   Updated: 2020/09/22 06:44:58 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "command.h"
#include "libft.h"
#include <unistd.h>

// Utility to bufferize any input to a processing function
// from any kind of input.
//
// Bear in mind that the buffer is static of size `4096`. This should enough.
// But if you need more you can increase it's size.
//
// Returns the status of the updater function.
bool				ssl_utils_message_proccess_input(
		t_ssl_cmd_data data,
		int in,
		void *ptr,
		size_t read_length,
		bool (*updater)(void*, const uint8_t *, ssize_t)
)
{
	uint8_t			buff[4096];
	ssize_t			red;
	t_ssl_cmd_arg	*opt;

	if (in >= 0)
	{
		red = read(in, buff, read_length);
		if (in == 0 && data.options[SSL_OPT_DIG_ID_PRT].enabled)
			write(1, buff, red);
		return (updater(ptr, buff, red));
	}
	opt = &data.options[SSL_OPT_DIG_ID_STR];
	red = ft_strlen(opt->args[-in - 1]);
	red = (red > read_length ? read_length : red);
	if (red <= 0)
		return (false);
	opt->args[-in - 1] += red;
	return (updater(ptr, (const uint8_t *)opt->args[-in - 1] - red, red));
}
