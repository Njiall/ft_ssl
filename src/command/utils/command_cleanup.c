/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command_cleanup.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: njiall <mbeilles@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/30 09:55:33 by njiall            #+#    #+#             */
/*   Updated: 2020/09/18 02:43:28 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "command.h"

/*
** Command data cleaner,
** 		for now it's role is to free alloacted data from
** 		the command data creation proccess, namely `ssl_command_handle_options`
*/

void				ssl_utils_command_cleanup(
		t_ssl_cmd_ctx *ctx
)
{
	// Frees custom options' arguments
	for (int i = SSL_OPT_SAFE_RANGE; i < SSL_OPT_MAX; ++i)
		// Checks for any command that takes option(s) and exists
		if (g_ssl_cmds[ctx->data.id].options[i - SSL_OPT_SAFE_RANGE].params
				&& ctx->data.options[i].args)
		{
			if (ctx->data.options[SSL_OPT_DEBUG].enabled)
				printf("\e[34mFreeing "
						"\e[0;1mdata\e[0;2m.\e[0;1moptions\e[0;2m[\e[0;35m0x%x "
						"\e[33m<=> "
						"\e[2;32m'\e[0;32m%s\e[2m'\e[0;2m].\e[0;1margs"
						"\e[0;2m: \e[0;35m%p\e[0m\n",
						i,
						g_ssl_cmds[ctx->data.id].options[i - SSL_OPT_SAFE_RANGE].aliases[0],
						ctx->data.options[i].args);
			free(ctx->data.options[i].args);
			ctx->data.options[i].args = NULL;
		}

	if (!g_type_table[g_ssl_cmds[ctx->data.id].type])
		return ;

	// Frees default type options' arguments
	for (int i = SSL_OPT_NONE; i < SSL_OPT_SAFE_RANGE; ++i)
		// Checks for any command that takes option(s) and exists
		if (g_type_table[g_ssl_cmds[ctx->data.id].type][i].params
				&& ctx->data.options[i].args)
		{

			if (ctx->data.options[SSL_OPT_DEBUG].enabled)
				printf("\e[34mFreeing "
						"\e[0;1mdata\e[0;2m.\e[0;1moptions\e[0;2m[\e[0;35m0x%x "
						"\e[33m<=> "
						"\e[2;32m'\e[0;32m%s\e[2m'\e[0;2m].\e[0;1margs"
						"\e[0;2m: \e[0;35m%p\e[0m\n",
						i,
						g_type_table[g_ssl_cmds[ctx->data.id].type][i].aliases[0],
						ctx->data.options[i].args);
			free(ctx->data.options[i].args);
			ctx->data.options[i].args = NULL;
		}

	if (ctx->alloc_argv)
	{
		if (ctx->data.options[SSL_OPT_DEBUG].enabled)
			printf("\e[34mFreeing argv\e[0;2m: \e[0;35m%p\e[0m\n", ctx->argv);
		free(ctx->argv);
	}
}
