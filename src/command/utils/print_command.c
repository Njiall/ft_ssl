/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_command.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/08 12:44:47 by mbeilles          #+#    #+#             */
/*   Updated: 2020/09/23 06:14:04 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "command.h"
#include <stdio.h>

/*
 * This file is used a debug mechanism to print a command handled by the utils.
 */

# define SHA2_HD_STR	"Secure Hash Algorithm v2"
# define DES_HD_STR		"Data Encryption Standard v1"
# define DES3_HD_STR	"3 x Data Encryption Standard v1"
# define DES_FT_STR		"- 56 bit key"
# define DES3_FT_STR	"- 168 bit key"

# define ENC_MD_CBC		"Cipher Block Chaining"
# define ENC_MD_CFB		"Cipher FeedBack"
# define ENC_MD_OFB		"Output FeedBack"
# define ENC_MD_ECB		"Electronic CodeBook"
# define ENC_MD_EDE		"Encrypt-Decrypt-Encrypt"

const char * const				g_cmd_id_name[SSL_ID_MAX] = {
	[SSL_ID_MD5] = "Message Digest v5 - 128 bits",

	[SSL_ID_SHA2_224] = SHA2_HD_STR" - 224 bits hash",
	[SSL_ID_SHA2_256] = SHA2_HD_STR" - 256 bits hash",
	[SSL_ID_SHA2_384] = SHA2_HD_STR" - 384 bits hash",
	[SSL_ID_SHA2_512] = SHA2_HD_STR" - 512 bits hash",

	[SSL_ID_DES_CBC] = DES_HD_STR" ["ENC_MD_CBC"] "DES_FT_STR,
	[SSL_ID_DES_CFB] = DES_HD_STR" ["ENC_MD_CFB"] "DES_FT_STR,
	[SSL_ID_DES_ECB] = DES_HD_STR" ["ENC_MD_ECB"] "DES_FT_STR,
	[SSL_ID_DES_EDE3] = DES3_HD_STR" ["ENC_MD_EDE"] "DES3_FT_STR,
};

t_ssl_cmd_status				ssl_command_print(
		const int argc,
		const char **argv,
		t_ssl_cmd_data data
)
{
	printf("\e[1mdata\e[0;2m: { \e[2;37m// Command data struct\e[0m\n");
	printf("  \e[0;2m.\e[0;1mcommand\e[0;2m: \e[32;2m'\e[0;32m%s\e[2m'\e[0;2m,\e[0m\n", data.cmd);
	printf("  \e[0;2m.\e[0;1mid\e[0;2m:\e[0m \e[35m0x%zx\e[0;2m,\e[0m\n", data.id);
	if (g_cmd_id_name[data.id])
		printf("  \e[0;2m.\e[0;1mid name\e[0;2m:\e[0m \e[2;32m'\e[0;32m%s\e[2m'\e[0;2m,\e[0m\n", g_cmd_id_name[data.id]);
	printf("  \e[0;2m.\e[0;1moptions\e[0;2m: [\e[0m\n");
	for (int i = 0; i < SSL_OPT_SAFE_RANGE + data.length; ++i)
	{
		if (data.options[i].enabled)
		{
			const char *word = (i >= SSL_OPT_SAFE_RANGE
					? g_ssl_cmds[data.id].options[i - SSL_OPT_SAFE_RANGE].aliases[0]
					: g_type_table[g_ssl_cmds[data.id].type][i].aliases[0]);
			printf("    \e[0;2m[\e[32;2m'\e[0;32m%s\e[2m'\e[0;2m, \e[0;35m0x%x\e[0;2m]: {\e[0m\n", word, i);
			printf("      \e[2m.\e[0;1menabled\e[2m:\e[0m \e[0;35m%lu\e[0;2m,\e[0m\n", data.options[i].enabled);
			if (data.options[i].length)
			{
				printf("      \e[2m.\e[0;1margs\e[2m:\e[0m \e[2m[\n");
				for (int j = 0; j < data.options[i].length; ++j)
					printf("        \e[2m[\e[0;35m%i\e[0;2m]:\e[32;2m'\e[0;32m%s\e[2m'\e[0;2m,\e[0m\n", j, data.options[i].args[j]);
				printf("      \e[0;2m],\e[0m\n");
				printf("      \e[2m.\e[0;1mlength\e[0;2m:\e[0m \e[35m%zu\e[0;2m,\e[0m\n", data.options[i].length);
			}
			printf("    \e[0;2m},\e[0m\n");
		}
	}
	for (int i = 0; i < SSL_OPT_SAFE_RANGE + data.length; ++i)
		if (!data.options[i].enabled
				&& (i >= SSL_OPT_SAFE_RANGE
					|| (g_type_table[g_ssl_cmds[data.id].type]
						&& g_type_table[g_ssl_cmds[data.id].type][i].aliases[0])))
		{
			const char *word = (i >= SSL_OPT_SAFE_RANGE
					? g_ssl_cmds[data.id].options[i - SSL_OPT_SAFE_RANGE].aliases[0]
					: g_type_table[g_ssl_cmds[data.id].type][i].aliases[0]);
			printf("    \e[0;2m[\e[32;2m'\e[0;32m%s\e[2m'\e[0;2m, \e[0;35m0x%x\e[0;2m]: {}\e[0m\n", word, i);
		}
	printf("  \e[0;2m],\e[0m\n");
	printf("  \e[0;2m.\e[0;1margs\e[0;2m: [%s", argc ? "\n" : "");
	for (int i = 0; i < argc; ++i)
		printf("    \e[2m[\e[0;35m%i\e[0;2m]: \e[32m'\e[0;32m%s\e[2m'\e[0;2m,\e[0m\n", i, argv[i]);
	printf("%s\e[0;2m],\e[0m\n", argc ? "  " : "");
	printf("  \e[0;2m.\e[0;1mlength\e[0;2m: \e[0;35m%i\e[0;2m,\e[0m\n", argc);
	printf("\e[0;2m} \e[0m\e[2;37m// Command data struct\e[0m\n");

	return ((t_ssl_cmd_status){.valid = true});
}

void							ssl_command_status_print(
		t_ssl_cmd_status status
)
{
	bool				tty = isatty(1);

	if (status.code && status.err[0] && tty)
	{
		printf("\e[2;37m[\e[0;31mSSL\e[2;37m]: \e[0;1;31mError\e[0;2m:\e[0m ");
		for (int i = 0;
				status.err[i] && i < sizeof(status.err) / sizeof(char*);
				++i)
			printf("%s", status.err[i]);
	}
}
