/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   generate_digest.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/13 12:59:42 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/13 13:33:11 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "command.h"
#include "digest.h"
#include "libft.h"

t_ssl_digest			ssl_genrate_digest(
		t_ssl_digest fprint,
		t_ssl_cmd_data data
)
{
	static const char	hex_format[] = "0123456789abcdef";
	static uint8_t		transform[4096];
	t_ssl_digest		output;

	if (data.options[SSL_OPT_DIG_ID_BIN].enabled
			&& !data.options[SSL_OPT_DIG_ID_HEX].enabled)
		return (fprint);
	else if (data.options[SSL_OPT_DIG_ID_COL].enabled)
	{
		int i = -1;
		while (++i < g_ssl_cmds[data.id].hash_length)
		{
			transform[i * 3 + 0] = hex_format[fprint.hash[i] >> 4];
			transform[i * 3 + 1] = hex_format[fprint.hash[i] & 0xf];
			if (i != g_ssl_cmds[data.id].hash_length - 1)
				transform[i * 3 + 2] = ':';
		}
		output.length = g_ssl_cmds[data.id].hash_length * 3 - 1;
	}
	else
	{
		int i = -1;
		while (++i < g_ssl_cmds[data.id].hash_length)
		{
			transform[i * 2 + 0] = hex_format[fprint.hash[i] >> 4];
			transform[i * 2 + 1] = hex_format[fprint.hash[i] & 0xf];
		}
		output.length = g_ssl_cmds[data.id].hash_length * 2;
	}
	output.hash = transform;
	return (output);
}
