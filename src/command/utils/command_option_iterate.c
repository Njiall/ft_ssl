#include "command.h"

const t_ssl_cmd_option		*ssl_command_option_iterate(
		t_ssl_cmd_id id,
		size_t *i
)
{
	// while there is no option we skip the option
	while (*i < SSL_OPT_SAFE_RANGE && !g_type_table[g_ssl_cmds[id].type][*i].aliases[0])
		++*i;
	// Check if there is any more opt
	// You could also check for SSL_OPT_MAX overflow but this is always smaller.
	if (*i >= g_ssl_cmds[id].count + SSL_OPT_SAFE_RANGE)
		return (NULL);

	const t_ssl_cmd_option *opt;
	// Check if this is a default option or a custom one.
	if (*i >= SSL_OPT_SAFE_RANGE)
		opt = &g_ssl_cmds[id].options[*i - SSL_OPT_SAFE_RANGE];
	else
		opt = &g_type_table[g_ssl_cmds[id].type][*i];
	++*i;
	return (opt);
}
