/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_digest.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/13 13:56:24 by mbeilles          #+#    #+#             */
/*   Updated: 2020/01/03 17:44:43 by njiall           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "command.h"
#include "libft.h"

static inline void	_print_silenced(
		const t_ssl_cmd_data * const data,
		const t_ssl_digest * const md,
		int out,
		const char *path
)
{
	write(out, md->hash, md->length);
	write(out, "\n", 1);
}

static inline void	_print_normal(
		const t_ssl_cmd_data * const data,
		const t_ssl_digest * const md,
		int out,
		const char *path
)
{
	write(out, g_ssl_cmds[data->id].hash_name,
			g_ssl_cmds[data->id].hash_name_length);
	write(out, "(", 1);
	write(out, path, ft_strlen(path));
	write(out, ")= ", 3);
	write(out, md->hash, md->length);
	write(out, "\n", 1);
}

static inline void	_print_reversed(
		const t_ssl_cmd_data * const data,
		const t_ssl_digest * const md,
		int out,
		const char *path
)
{
	write(out, md->hash, md->length);
	write(out, " *", 2);
	write(out, path, ft_strlen(path));
	write(out, "\n", 1);
}

static inline void	_print_binary(
		const t_ssl_cmd_data * const data,
		const t_ssl_digest * const md,
		int out,
		const char *path
)
{
	write(out, md->hash, md->length);
}

void				ssl_utils_print_digest(
		t_ssl_cmd_data data,
		t_ssl_digest md,
		int out,
		const char *path
)
{
	if (data.options[SSL_OPT_DIG_ID_BIN].enabled)
		_print_binary(&data, &md, out, path);
	else if (data.options[SSL_OPT_DIG_ID_SIL].enabled)
		_print_silenced(&data, &md, out, path);
	else if (data.options[SSL_OPT_DIG_ID_COR].enabled)
		_print_reversed(&data, &md, out, path);
	else if (path && !ft_strequ(path, "stdin"))
		_print_normal(&data, &md, out, path);
	else
		_print_silenced(&data, &md, out, path);
}
