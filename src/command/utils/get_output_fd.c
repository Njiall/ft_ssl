/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_output_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/13 13:41:43 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/30 12:29:24 by njiall           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <fcntl.h>
#include "command.h"
#include "ssl_common.h"
#include "libft.h"

bool					get_output_fds(
		t_ssl_cmd_data data,
		t_fd_list *out
)
{
	if (data.options[SSL_OPT_DIG_ID_OUT].length <= 0)
	{
		out->length = 1;
		if (!(out->list = malloc(sizeof(t_fd_type))))
			ft_exit(SSL_ERR_FAILED_ALLOC, "No space left on device.\n", NULL);
		out->list[0] = 1;
		return (true);
	}

	out->length = data.options[SSL_OPT_DIG_ID_OUT].length;
	// If allocation failed return empty list
	if (!(out->list = malloc(out->length * sizeof(t_fd_type))))
		ft_exit(SSL_ERR_FAILED_ALLOC, "No space left on device.\n", NULL);

	// Loops over arguments;
	for (size_t i = 0; i < out->length; ++i)
		if (0 > (out->list[i] = open(data.options[SSL_OPT_DIG_ID_OUT].args[i],
				O_WRONLY | O_TRUNC | O_CREAT, 0644)))
			return (false);
	return (true);
}

int						get_output_fd(
		t_ssl_cmd_data data
)
{
	int					out;

	if (data.options[SSL_OPT_DIG_ID_OUT].enabled
			&& data.options[SSL_OPT_DIG_ID_OUT].length > 0)
	{
		out = open(data.options[SSL_OPT_DIG_ID_OUT].args[0],
				O_WRONLY | O_TRUNC | O_CREAT, 0644);
		if (out < 0)
			return (-1);
		return (out);
	}
	else
		return (1);
}
