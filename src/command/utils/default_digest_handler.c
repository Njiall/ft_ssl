/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   default_digest_handler.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/13 14:08:46 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/31 13:31:15 by njiall           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#include "command.h"
#include "libft.h"

static t_ssl_cmd_status		(*g_digests[SSL_ID_MAX])(
		t_ssl_cmd_data data,
		const char *path,
		int in,
		int out
) = {
	[SSL_ID_SHA2_224] = ssl_sha2_224_digest,
	[SSL_ID_SHA2_256] = ssl_sha2_256_digest,
	[SSL_ID_SHA2_384] = ssl_sha2_384_digest,
	[SSL_ID_SHA2_512] = ssl_sha2_512_digest,
	[SSL_ID_MD5] = ssl_md5_digest,
};

static inline char			*_get_formatted_str_arg(const char *arg)
{
	char					*s;
	size_t					l;

	l = ft_strlen(arg);
	s = malloc(l + 3);
	ft_memcpy(s + 1, arg, l);
	s[0] = '"';
	s[l + 1] = '"';
	s[l + 2] = '\0';
	return (s);
}

t_ssl_cmd_status			ssl_utils_digest_handler(
		const int argc,
		const char **argv,
		t_ssl_cmd_data data
)
{
	t_ssl_cmd_status			status;
	t_ssl_cmd_status			temp;
	int							in;
	int							out;

	if ((out = get_output_fd(data)) < 0)
		return ((t_ssl_cmd_status){
				.code = SSL_ERR_FILE_NOT_FOUND,
				.err = {
					data.options[SSL_OPT_DIG_ID_OUT].args[0],
					": Permisson denied.\n",
				},
		});
	status = (t_ssl_cmd_status){.valid = true};

	if ((argc <= 0 && !data.options[SSL_OPT_DIG_ID_STR].enabled)
			|| data.options[SSL_OPT_DIG_ID_PRT].enabled)
	{
		temp = g_digests[data.id](data, "stdin", 0, out);
		if (out > 1)
			close(out);
		if (!temp.valid)
			return (temp);
	}

	// String passed as an argument
	if (data.options[SSL_OPT_DIG_ID_STR].enabled)
		for (int i = 0; i < data.options[SSL_OPT_DIG_ID_STR].length; ++i)
	{
		// TODO Fix method for passing strings to read,
		// 		i.e.: Use polymorphism, like a tagged union.
		char *s = _get_formatted_str_arg(data.options[SSL_OPT_DIG_ID_STR].args[i]);
		temp = g_digests[data.id](data, s, -(i + 1), out);
		free(s);
		if (!temp.valid)
		{
			if (out > 1)
				close(out);
			if (!temp.valid)
				return (temp);
		}
	}

	// Handle arguments
	for (int i = 0; i < argc; ++i)
	{
		if ((in = open(argv[i], O_RDONLY)) < 0)
		{
			ssl_command_status_print((t_ssl_cmd_status){
					.code = SSL_ERR_FILE_NOT_FOUND,
					.err = {argv[i], ": No such file or directory\n"},
			});
			status = (t_ssl_cmd_status){.code = SSL_ERR_FILE_NOT_FOUND};
			continue ;
		}
		temp = g_digests[data.id](data, argv[i], in, out);
		close(in);
		if (!temp.valid)
		{
			if (out > 1)
				close(out);
			return (temp);
		}
	}
	if (out > 1)
		close(out);
	return (status);
}
