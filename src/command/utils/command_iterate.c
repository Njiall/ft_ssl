#include "command.h"

const t_ssl_cmd		*ssl_command_iterate(
		t_ssl_cmd_id *i
)
{
	// while there is no option we skip the option
	while (*i < sizeof(g_ssl_cmds) / sizeof(*g_ssl_cmds) && !g_ssl_cmds[*i].type)
		++*i;
	// Check if there is any more opt
	// You could also check for SSL_OPT_MAX overflow but this is always smaller.
	if (*i >= sizeof(g_ssl_cmds) / sizeof(*g_ssl_cmds))
		return (NULL);
	++*i;
	return (g_ssl_cmds + *i - 1);
}
