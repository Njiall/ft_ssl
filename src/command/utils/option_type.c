/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   option_type.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: njiall <mbeilles@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/30 10:03:15 by njiall            #+#    #+#             */
/*   Updated: 2020/05/29 16:21:09 by njiall           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "command.h"
#include "libft.h"

static inline bool				_is_option(
		const char * const arg,
		const t_ssl_cmd_option opt
)
{
	for (size_t i = 0;
			i < sizeof(opt.aliases) / sizeof(*opt.aliases)
			&& opt.aliases[i];
			++i)
	{
		if (ft_strlen(arg) == opt.length[i]
				&& ft_strnequ(arg, opt.aliases[i], opt.length[i]))
			return (true);
	}
	return (false);
}

/*
** Boolean check to know if current argument from the context is
** 		a type option.
**
** Input:
** 		- command context
** 		- option index
*/

bool							ssl_is_default_option(
		t_ssl_cmd_ctx *ctx,
		size_t id
)
{
	const t_ssl_cmd_option		*opts;

	// Gets command type default options
	opts = g_type_table[g_ssl_cmds[ctx->data.id].type];

	// Checks if index makes sense and if this is the option we search
	return (id < SSL_OPT_SAFE_RANGE && opts
			&& _is_option(ctx->argv[ctx->index], opts[id]));
}

/*
** Boolean check to know if current argument from the context is
** 		a custom option.
**
** Input:
** 		- command context
** 		- option index
*/

bool							ssl_is_custom_option(
		t_ssl_cmd_ctx *ctx,
		size_t id
)
{
	const t_ssl_cmd_option		*opts;

	// Gets comamnd type default options
	opts = g_ssl_cmds[ctx->data.id].options;
	// Checks if index makes sense and if this is the option we search
	// We have to loop on the different aliases.
	// So we loop over the entire table or stop if we encounter a NULL.
	if (id >= SSL_OPT_SAFE_RANGE && opts)
	{
		// Substract safe range for type options to index since custom options
		// are 0 indexed but stored offset with safe range
		id -= SSL_OPT_SAFE_RANGE;
		if (_is_option(ctx->argv[ctx->index], opts[id]))
			return (true);
	}
	return (false);
}
