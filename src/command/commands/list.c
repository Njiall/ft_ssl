#include "command.h"
#include "libft.h"

t_ssl_cmd_status	ssl_command_list_digests(
		const int argc,
		const char **argv,
		t_ssl_cmd_data data
)
{
	if (argc > 0)
		return ((t_ssl_cmd_status){
				.err = {"This command doesn't take arguments.\n"},
				.code = SSL_ERR_WRONG_ARGUMENT,
		});
	return((t_ssl_cmd_status){.valid = true});
}

t_ssl_cmd_status	ssl_command_list_ciphers(
		const int argc,
		const char **argv,
		t_ssl_cmd_data data
)
{
	if (argc > 0)
		return ((t_ssl_cmd_status){
				.err = {"This command doesn't take arguments.\n"},
				.code = SSL_ERR_WRONG_ARGUMENT,
		});
	return((t_ssl_cmd_status){.valid = true});
}

t_ssl_cmd_status	ssl_command_list_encription(
		const int argc,
		const char **argv,
		t_ssl_cmd_data data
)
{
	if (argc > 0)
		return ((t_ssl_cmd_status){
				.err = {"This command doesn't take arguments.\n"},
				.code = SSL_ERR_WRONG_ARGUMENT,
		});
	return((t_ssl_cmd_status){.valid = true});
}

static t_ssl_cmd_handler (*g_list[3]) = {
	[0] = &ssl_command_list_digests,
	[1] = &ssl_command_list_ciphers,
	[2] = &ssl_command_list_encription,
};

t_ssl_cmd_status	ssl_command_list(
		const int argc,
		const char **argv,
		t_ssl_cmd_data data
)
{
	t_ssl_cmd_status status = (t_ssl_cmd_status){.valid = true};
	for (size_t i = 0; i < sizeof(g_list) / sizeof(*g_list) && status.valid; ++i)
		if (data.options[i].enabled)
			status = g_list[i](argc, argv, data);
	return (status);
}
