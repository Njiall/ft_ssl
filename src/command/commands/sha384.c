/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha384.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/13 11:10:55 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/13 19:05:29 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "command.h"
#include "sha2.h"

t_ssl_cmd_status				ssl_sha2_384_digest(
		t_ssl_cmd_data data,
		const char *path,
		int in,
		int out
)
{
	t_sha2_ctx					ctx;

	// Process sha2 hash on file
	ssl_sha2_384_init(&ctx);
	while (ssl_utils_message_proccess_input(
				data, in, &ctx,
				1024 / 8,
				(bool(*)(void*,const uint8_t*,ssize_t))&ssl_sha2_1024_update_data
	))
		ssl_sha2_512_vector(&ctx);

	// Outputs
	ssl_utils_print_digest(data,
			ssl_genrate_digest(ssl_sha2_384_final_bin(&ctx), data),
			out, path);

	return ((t_ssl_cmd_status){.valid = true});
}
