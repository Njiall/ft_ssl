/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/08 12:41:34 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/21 12:52:01 by njiall           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

#include "command.h"
#include "md5.h"
#include "libft.h"

t_ssl_cmd_status				ssl_md5_digest(
		t_ssl_cmd_data data,
		const char *path,
		int in,
		int out
)
{
	t_md5_ctx					ctx;
	uint8_t						buf[sizeof(ctx.data)];
	ssize_t						red;

	// Process md5 hash on file
	ssl_md5_init(&ctx);
	while (ssl_utils_message_proccess_input(
				data, in, &ctx,
				512 / 8,
				(bool(*)(void*,const uint8_t*,ssize_t))&ssl_md5_update_data
	))
		ssl_md5_vector(&ctx);

	ssl_utils_print_digest(data,
			ssl_genrate_digest(ssl_md5_final_bin(&ctx), data),
			out, path);
	return ((t_ssl_cmd_status){.valid = true});
}
