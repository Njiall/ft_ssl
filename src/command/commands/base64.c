/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base64.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: njiall <mbeilles@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/03 19:45:38 by njiall            #+#    #+#             */
/*   Updated: 2020/01/11 01:56:22 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

#include "command.h"
#include "libft.h"
#include "ssl_common.h"

/*
** These are the tables that helps lookup the conversion,
** 		from encoded shape to plain and vice versa.
**
** .        'w'        .        'a'        .
** |       0x77        |       0x61        |
** |-------------.-----+---------.---------+-----.-------------.
** | 0 1 1 1 0 1 | 1 1 ' 0 1 1 0 | 0 0 0 1 ' 0 0 | 0 0 0 0 0 0 |
** |-------------+---------------+---------------+-------------|
** | [29] = "d"  |   [54] = "2"  |   [4] = "E"   | pad -> "="  |
** `-------------`---------------`---------------`-------------`
*/

static const bool				g_ssl_base64_fmt[256] = {
	['A' ... 'Z'] = true,
	['a' ... 'z'] = true,
	['0' ... '9'] = true,
	['+'] = true,
	['.'] = true,
	['-'] = true,
	[','] = true,
	['/'] = true,
	['='] = true,
};

static const uint8_t			g_ssl_base64_dec[256] = {
	['A'] = 0,  ['B'] = 1,  ['C'] = 2,  ['D'] = 3,  ['E'] = 4,  ['F'] = 5,
	['G'] = 6,  ['H'] = 7,  ['I'] = 8,  ['J'] = 9,  ['K'] = 10, ['L'] = 11,
	['M'] = 12, ['N'] = 13, ['O'] = 14, ['P'] = 15, ['Q'] = 16, ['R'] = 17,
	['S'] = 18, ['T'] = 19, ['U'] = 20, ['V'] = 21, ['W'] = 22, ['X'] = 23,
	['Y'] = 24, ['Z'] = 25,

	['a'] = 26, ['b'] = 27, ['c'] = 28, ['d'] = 29, ['e'] = 30, ['f'] = 31,
	['g'] = 32, ['h'] = 33, ['i'] = 34, ['j'] = 35, ['k'] = 36, ['l'] = 37,
	['m'] = 38, ['n'] = 39, ['o'] = 40, ['p'] = 41, ['q'] = 42, ['r'] = 43,
	['s'] = 44, ['t'] = 45, ['u'] = 46, ['v'] = 47, ['w'] = 48, ['x'] = 49,
	['y'] = 50, ['z'] = 51,

	['0'] = 52, ['1'] = 53, ['2'] = 54, ['3'] = 55, ['4'] = 56,
	['5'] = 57, ['6'] = 58, ['7'] = 59, ['8'] = 60, ['9'] = 61,

	['+'] = 62, ['.'] = 62, ['-'] = 62,
	[','] = 63, ['/'] = 63,
	['='] = 0,
};

static const uint8_t			g_ssl_base64_enc[65] = {
	[0]  = 'A', [1]  = 'B', [2]  = 'C', [3]  = 'D', [4]  = 'E', [5]  = 'F',
	[6]  = 'G', [7]  = 'H', [8]  = 'I', [9]  = 'J', [10] = 'K', [11] = 'L',
	[12] = 'M', [13] = 'N', [14] = 'O', [15] = 'P', [16] = 'Q', [17] = 'R',
	[18] = 'S', [19] = 'T', [20] = 'U', [21] = 'V', [22] = 'W', [23] = 'X',
	[24] = 'Y', [25] = 'Z',

	[26] = 'a', [27] = 'b', [28] = 'c', [29] = 'd', [30] = 'e', [31] = 'f',
	[32] = 'g', [33] = 'h', [34] = 'i', [35] = 'j', [36] = 'k', [37] = 'l',
	[38] = 'm', [39] = 'n', [40] = 'o', [41] = 'p', [42] = 'q', [43] = 'r',
	[44] = 's', [45] = 't', [46] = 'u', [47] = 'v', [48] = 'w', [49] = 'x',
	[50] = 'y', [51] = 'z',

	[52] = '0', [53] = '1', [54] = '2', [55] = '3', [56] = '4',
	[57] = '5', [58] = '6', [59] = '7', [60] = '8', [61] = '9',

	[62] = '+', [63] = '/', [64] = '='
};

static inline void				_encode_buffer(
		uint8_t buf[3],
		char enc[4],
		size_t red
)
{
	// Encodes chars
	enc[0] = g_ssl_base64_enc[(buf[0] & 0xFC) >> 2];
	enc[1] = g_ssl_base64_enc[(buf[0] & 0x03) << 4 | (buf[1] & 0xF0) >> 4];
	enc[2] = g_ssl_base64_enc[(buf[1] & 0x0F) << 2 | (buf[2] & 0xC0) >> 6];
	enc[3] = g_ssl_base64_enc[(buf[2] & 0x3F)];

	// Pads with padding symbol
	if (red < 3)
		for (int i = 0; i < 3 - red; ++i)
			enc[3 - i] = g_ssl_base64_enc[64];
}

static inline void				_decode_buffer(
		uint8_t buf[4],
		char dec[3],
		size_t red
)
{
	uint32_t					n;

	n = 0;
	for (int i = 0; i < red; ++i)
		n |= g_ssl_base64_dec[buf[i]] << (3 - i) * 6;
	// Standadisation of notation will be optimized later
	dec[0] = (n >> 0x10) & 0xFF;
	dec[1] = (n >> 0x08) & 0xFF;
	dec[2] = (n >> 0x00) & 0xFF;
}

static inline void				_encode_base64(
		t_ssl_cmd_data data,
		int in,
		const int *out,
		const size_t length
)
{
	// We will be grouping chars by 3 as 6 bits are required to encode base 64
	// and the closest dividing mutiple is 4. as 4 x 3 = 6 x 2 = 12.
	uint8_t						buf[3];
	// Char string that contains the encoded part.
	char						enc[4];
	size_t						red;

	ft_memset(buf, 0, sizeof(buf));
	for (size_t pad = 0; (red = read(in, buf, sizeof(buf))) > 0; pad += 1)
	{

		_encode_buffer(buf, enc, red);
		// Prints to output
		write(out[0], enc, sizeof(enc));
		if ((pad) % 16 == 15)
			write(out[0], "\n", 1);

		// Sanitize buffer
		ft_memset(buf, 0, sizeof(buf));
	}
	write(out[0], "\n", 1);
}

static inline bool				_is_base64(
		uint8_t buf[4],
		size_t red
)
{
	for (int i = 0; i < red; ++i)
		if (buf[i] && !g_ssl_base64_fmt[buf[0]])
			return (false);
	return (true);
}

static inline void				_decode_base64(
		t_ssl_cmd_data data,
		int in,
		const int *out,
		const size_t length
)
{
	uint8_t						buf[4];
	char						dec[3];
	size_t						red;

	for (size_t pad = 0;
			ft_memset(buf, 0, sizeof(buf))
			&& (red = read(in, buf, sizeof(buf))) > 0;
			pad += 1)
	{
		// If the buffer contains non valid chars we dump it.
		if (!_is_base64(buf, red))
			continue;

		printf("in: [%.*s]\n", (int)red, buf);
		// This is padding
		if ((g_ssl_base64_enc[64] == buf[2] && g_ssl_base64_enc[64] == buf[3])
				|| g_ssl_base64_enc[64] == buf[3])
		{
			red = 3 - (g_ssl_base64_enc[64] == buf[2]);
			printf("wow\n");
		}
		printf("red: %zu\n", red);

		// Then decode buffer
		_decode_buffer(buf, dec, red);
		// Prints to output
		write(out[0], dec, red - 1);
	}
}

t_ssl_cmd_status				ssl_base64(
		const int argc,
		const char **argv,
		t_ssl_cmd_data data
)
{
	int						in;
	int						out = 1;
	bool					mode;
	t_ssl_cmd_status		status;

	status = (t_ssl_cmd_status){.valid = true};
	mode = (data.options[SSL_OPT_B64_DEC].enabled
			&& !data.options[SSL_OPT_B64_ENC].enabled);

	if (argc < 1)
		if (!mode)
			_encode_base64(data, 0, (int[]){ out }, 1);
		else
			_decode_base64(data, 0, (int[]){ out }, 1);
	else
		for (int i = 0; i < argc; ++i)
		{
			if ((in = open(argv[i], O_RDONLY)) < 0)
			{
				ssl_command_status_print((t_ssl_cmd_status){
						.code = SSL_ERR_FILE_NOT_FOUND,
						.err = {argv[i], ": No such file or directory\n"},
				});
				status = (t_ssl_cmd_status){.code = SSL_ERR_FILE_NOT_FOUND};
				continue ;
			}
			if (!mode)
				_encode_base64(data, in, (int[]){ out }, 1);
			else
				_decode_base64(data, in, (int[]){ out }, 1);
			close(in);
		}

	return (status);
}
