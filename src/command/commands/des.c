/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: njiall <mbeilles@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/03 19:45:38 by njiall            #+#    #+#             */
/*   Updated: 2020/01/05 19:09:04 by njiall           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

#include "command.h"
#include "libft.h"
#include "des.h"

t_ssl_cmd_status				ssl_des(
		const int argc,
		const char **argv,
		t_ssl_cmd_data data
)
{
	uint8_t						buf[10];
	ssize_t						red;

	char * pass = getpass("des-cbc password: ");
	printf("password: '%s'\n", pass);

	// Process md5 hash on file
	return ((t_ssl_cmd_status){.valid = true});
}
