#include "command.h"

t_ssl_cmd_status		ssl_command_usage(
		const int argc,
		const char **argv,
		t_ssl_cmd_data data
)
{
	bool				all = !!data.options[SSL_OPT_SAFE_RANGE + 0].enabled;

	// If help asked
	if (argc == 0)
	{
		ssl_command_print_general_usage(data.cmd, all);
		return ((t_ssl_cmd_status){.valid = true});
	}

	bool				cmd_done[SSL_ID_MAX] = {};
	// Loops over help command arguemnts
	for (int i = 0; i < argc; ++i)
	{
		// Find targeted command
		t_ssl_cmd_id id = ssl_command_get_id(argv[i]);
		// If not a valid command name
		if (id >= SSL_ID_MAX || id <= SSL_ID_NONE)
			return ((t_ssl_cmd_status){
				.code = SSL_ERR_HELP_INVALID_CMD,
				.err = {"help: '", argv[i], "' is not a valid command.\n"}
			});
		// Avoid printing useless command usages
		if (cmd_done[id])
			continue ;
		else
			cmd_done[id] = true;

		ssl_command_print_usage(id, all);
	}

	return ((t_ssl_cmd_status){.valid = true});
}
