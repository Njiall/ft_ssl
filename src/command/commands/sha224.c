/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha224.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/11 10:07:14 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/13 19:04:23 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "command.h"
#include "sha2.h"
#include "libft.h"

t_ssl_cmd_status				ssl_sha2_224_digest(
		t_ssl_cmd_data data,
		const char *path,
		int in,
		int out
)
{
	t_sha2_ctx					ctx;
	uint8_t						buff[sizeof(ctx.data)];
	ssize_t						red;

	// Process sha2 hash on file
	ssl_sha2_224_init(&ctx);
	while (ssl_utils_message_proccess_input(
				data, in, &ctx,
				512 / 8,
				(bool(*)(void*,const uint8_t*,ssize_t))&ssl_sha2_512_update_data
	))
		ssl_sha2_256_vector(&ctx);

	// Outputs
	ssl_utils_print_digest(data,
			ssl_genrate_digest(ssl_sha2_224_final_bin(&ctx), data),
			out, path);

	return ((t_ssl_cmd_status){.valid = true});
}
