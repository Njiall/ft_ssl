/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha2_224_init.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/11 17:22:41 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/11 17:23:27 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha2.h"


void					ssl_sha2_224_init(t_sha2_ctx *ctx)
{
	*ctx = (t_sha2_ctx){
		.insert_pad = true,
		.last_block = true,
		.md = (t_sha2_hash){
			.state = {
				0xc1059ed8,
				0x367cd507,
				0x3070dd17,
				0xf70e5939,
				0xffc00b31,
				0x68581511,
				0x64f98fa7,
				0xbefa4fa4,
			},
		},
	};
}
