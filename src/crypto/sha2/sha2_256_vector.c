/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha2_256_vector.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/11 14:25:31 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/13 17:38:10 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha2.h"
#include "libft.h"

static uint32_t			g_elem[64] = {
	0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
	0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
	0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
	0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
	0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
	0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
	0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
	0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
	0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
	0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
	0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
	0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
	0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
	0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
	0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
	0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2,
};

static inline uint32_t	_right_rotate(uint32_t a, uint32_t b)
{
	return ((a >> b) | (a << (32 - b)));
}

static inline uint32_t	_left_rotate(uint32_t a, uint32_t b)
{
	return ((a << b) | (a >> (32 - b)));
}

void					ssl_sha2_256_vector(t_sha2_ctx *ctx)
{
	uint32_t			schedule[512 / 8];
	uint32_t			reg[512 / (8 * sizeof(uint32_t))];
	uint32_t			t1;
	uint32_t			t2;
	int i;

	// Format scheduler data
	for (i = 0; i < 16; ++i)
		schedule[i] = 0
			| (ctx->data[i * 4 + 0] << 24)
			| (ctx->data[i * 4 + 1] << 16)
			| (ctx->data[i * 4 + 2] << 8)
			| (ctx->data[i * 4 + 3]);
	for (; i < 64; ++i)
		schedule[i] = 0
			+ (_right_rotate(schedule[i - 15],  7) ^ _right_rotate(schedule[i - 15], 18) ^ (schedule[i - 15] >>  3)) + schedule[i - 16]
			+ (_right_rotate(schedule[i -  2], 17) ^ _right_rotate(schedule[i -  2], 19) ^ (schedule[i -  2] >> 10)) + schedule[i -  7];

	// Init reg with stored state
	ft_memcpy(reg, ctx->md.state, sizeof(reg));

	for (i = 0; i < 64; ++i)
	{
		t1 = reg[7] + g_elem[i] + schedule[i]
			+ (_right_rotate(reg[4], 6) ^ _right_rotate(reg[4], 11) ^ _right_rotate(reg[4], 25)) // S0
			+ (((reg[4]) & (reg[5])) ^ (~(reg[4]) & (reg[6]))); // ch
		t2 = (_right_rotate(reg[0], 2) ^ _right_rotate(reg[0], 13) ^ _right_rotate(reg[0], 22)) // S1
			+ (((reg[0]) & (reg[1])) ^ ((reg[0]) & (reg[2])) ^ ((reg[1]) & (reg[2]))); // maj
		reg[7] = reg[6];
		reg[6] = reg[5];
		reg[5] = reg[4];
		reg[4] = reg[3] + t1;
		reg[3] = reg[2];
		reg[2] = reg[1];
		reg[1] = reg[0];
		reg[0] = t1 + t2;
	}

	// Update state with reg registers
	for (i = 0; i < sizeof(reg) / sizeof(uint32_t); ++i)
		ctx->md.state[i] += reg[i];
}
