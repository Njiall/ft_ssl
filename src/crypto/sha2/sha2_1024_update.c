/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha2_1024_update.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/13 08:31:01 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/13 18:57:16 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

#include "sha2.h"
#include "libft.h"

bool					ssl_sha2_1024_update_data(
		t_sha2_ctx *ctx,
		const uint8_t *buff,
		ssize_t len
)
{
	ft_memcpy(ctx->ldata, buff, len);
	ft_memset(ctx->ldata + len, 0, sizeof(ctx->ldata) - len);
	if (len <= 0 && !ctx->last_block)
		return (false);
	ctx->ldata_len += len;
	if (len != sizeof(ctx->ldata) && ctx->insert_pad)
	{
		ctx->ldata[len] = 0b10000000;
		ctx->insert_pad = false;
	}
	if (len != sizeof(ctx->ldata)
			&& len <= sizeof(ctx->ldata) - sizeof(ctx->ldata_len) - 1
			&& ctx->last_block)
	{
		ctx->lbit_len = ctx->ldata_len * 8;
		// Puts len as big endian in data
		// Since 128 bit int, convert to table and invert each byte
		uint8_t *t = (uint8_t[sizeof(ctx->lbit_len)]){
			[15] = ((uint8_t*)&ctx->lbit_len)[0],
			[14] = ((uint8_t*)&ctx->lbit_len)[1],
			[13] = ((uint8_t*)&ctx->lbit_len)[2],
			[12] = ((uint8_t*)&ctx->lbit_len)[3],
			[11] = ((uint8_t*)&ctx->lbit_len)[4],
			[10] = ((uint8_t*)&ctx->lbit_len)[5],
			[9]  = ((uint8_t*)&ctx->lbit_len)[6],
			[8]  = ((uint8_t*)&ctx->lbit_len)[7],
			[7]  = ((uint8_t*)&ctx->lbit_len)[8],
			[6]  = ((uint8_t*)&ctx->lbit_len)[9],
			[5]  = ((uint8_t*)&ctx->lbit_len)[10],
			[4]  = ((uint8_t*)&ctx->lbit_len)[11],
			[3]  = ((uint8_t*)&ctx->lbit_len)[12],
			[2]  = ((uint8_t*)&ctx->lbit_len)[13],
			[1]  = ((uint8_t*)&ctx->lbit_len)[14],
			[0]  = ((uint8_t*)&ctx->lbit_len)[15],
		};
		ft_memcpy(ctx->ldata + sizeof(ctx->ldata) - sizeof(ctx->lbit_len),
				t, sizeof(ctx->lbit_len));
		ctx->last_block = false;
	}
	return (true);
}
