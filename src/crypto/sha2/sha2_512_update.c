/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha2_512_update.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/11 14:26:49 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/13 19:01:35 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

#include "sha2.h"
#include "libft.h"

bool					ssl_sha2_512_update_data(
		t_sha2_ctx *ctx,
		const uint8_t *buff,
		ssize_t len
)
{
	ft_memcpy(ctx->data, buff, len);
	ft_memset(ctx->data + len, 0, sizeof(ctx->data) - len);
	if (len <= 0 && !ctx->last_block)
		return (false);
	ctx->data_len += len;
	if (len != sizeof(ctx->data) && ctx->insert_pad)
	{
		ctx->data[len] = 0b10000000;
		ctx->insert_pad = false;
	}
	if (len != sizeof(ctx->data)
			&& len <= sizeof(ctx->data) - sizeof(ctx->data_len) - 1
			&& ctx->last_block)
	{
		ctx->bit_len = ctx->data_len * 8;
		// puts len as big endian in data
		// constant time
		((uint64_t*)ctx->data)[7] = 0llu
			| (ctx->bit_len & 0xff00000000000000) >> (48 + 8)
			| (ctx->bit_len & 0x00ff000000000000) >> (32 + 8)
			| (ctx->bit_len & 0x0000ff0000000000) >> (16 + 8)
			| (ctx->bit_len & 0x000000ff00000000) >> (0  + 8)
			| (ctx->bit_len & 0x00000000ff000000) << (0  + 8)
			| (ctx->bit_len & 0x0000000000ff0000) << (16 + 8)
			| (ctx->bit_len & 0x000000000000ff00) << (32 + 8)
			| (ctx->bit_len & 0x00000000000000ff) << (48 + 8);
		ctx->last_block = false;
	}

	return (true);
}
