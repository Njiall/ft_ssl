/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha2_512_final_bin.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/13 11:11:58 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/13 11:12:45 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "digest/sha2.h"
#include "digest.h"
#include "libft.h"

t_ssl_digest			ssl_sha2_512_final_bin(t_sha2_ctx *ctx)
{
	static const char	format[] = "0123456789abcdef";
	static uint8_t		final[512 / 8];
	size_t				i;

	// Reverse endianess of hash
	i = -1;
	while (++i < sizeof(final) / sizeof(uint64_t))
		((uint64_t*)final)[i] = 0llu
			| (ctx->md.lstate[i] & 0xff00000000000000) >> (48 + 8)
			| (ctx->md.lstate[i] & 0x00ff000000000000) >> (32 + 8)
			| (ctx->md.lstate[i] & 0x0000ff0000000000) >> (16 + 8)
			| (ctx->md.lstate[i] & 0x000000ff00000000) >> (0  + 8)
			| (ctx->md.lstate[i] & 0x00000000ff000000) << (0  + 8)
			| (ctx->md.lstate[i] & 0x0000000000ff0000) << (16 + 8)
			| (ctx->md.lstate[i] & 0x000000000000ff00) << (32 + 8)
			| (ctx->md.lstate[i] & 0x00000000000000ff) << (48 + 8);
	return ((t_ssl_digest){.hash = final, .length = sizeof(final)});
}
