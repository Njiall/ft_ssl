/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha2_256_init.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/11 09:01:33 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/11 14:27:32 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

#include "digest/sha2.h"

void					ssl_sha2_256_init(t_sha2_ctx *ctx)
{
	*ctx = (t_sha2_ctx){
		.insert_pad = true,
		.last_block = true,
		.md = (t_sha2_hash){
			.state = {
				0x6a09e667,
				0xbb67ae85,
				0x3c6ef372,
				0xa54ff53a,
				0x510e527f,
				0x9b05688c,
				0x1f83d9ab,
				0x5be0cd19,
			},
		},
	};
}
