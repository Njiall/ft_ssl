/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha2_256_final_bin.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/11 14:27:26 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/13 15:40:15 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "digest/sha2.h"
#include "digest.h"
#include "libft.h"

t_ssl_digest			ssl_sha2_256_final_bin(t_sha2_ctx *ctx)
{
	static uint8_t		final[256 / 8];
	size_t				i;

	// Reverse endianess of hash
	i = -1;
	while (++i < sizeof(final) / sizeof(uint32_t))
		((uint32_t*)final)[i] = 0llu
			| (ctx->md.state[i] & 0xff000000) >> 24
			| (ctx->md.state[i] & 0x00ff0000) >> 8
			| (ctx->md.state[i] & 0x0000ff00) << 8
			| (ctx->md.state[i] & 0x000000ff) << 24;

	return ((t_ssl_digest){.hash = final, .length = sizeof(final)});
}
