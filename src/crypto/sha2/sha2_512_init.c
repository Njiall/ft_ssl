/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha2_512_init.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/13 08:11:03 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/13 08:24:29 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha2.h"

void					ssl_sha2_512_init(t_sha2_ctx *ctx)
{
	*ctx = (t_sha2_ctx){
		.insert_pad = true,
		.last_block = true,
		.md = (t_sha2_hash){
			.lstate = {
				0x6a09e667f3bcc908,
				0xbb67ae8584caa73b,
				0x3c6ef372fe94f82b,
				0xa54ff53a5f1d36f1,
				0x510e527fade682d1,
				0x9b05688c2b3e6c1f,
				0x1f83d9abfb41bd6b,
				0x5be0cd19137e2179,
			},
		},
	};
}
