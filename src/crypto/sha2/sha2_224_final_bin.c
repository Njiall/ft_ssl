/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha2_224_final_bin.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/11 17:27:53 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/13 14:41:34 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "digest/sha2.h"
#include "digest.h"
#include "libft.h"

t_ssl_digest			ssl_sha2_224_final_bin(t_sha2_ctx *ctx)
{
	static uint8_t		final[224 / 8];
	size_t				i;

	// Reverse endianess of hash
	i = -1;
	while (++i < 7)
		((uint32_t*)final)[i] = 0llu
			| (ctx->md.state[i] & 0xff000000) >> 24
			| (ctx->md.state[i] & 0x00ff0000) >> 8
			| (ctx->md.state[i] & 0x0000ff00) << 8
			| (ctx->md.state[i] & 0x000000ff) << 24;

	return ((t_ssl_digest){.hash = final, .length = sizeof(final)});
}
