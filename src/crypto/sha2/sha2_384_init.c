/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha2_384_init.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/13 11:09:22 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/13 11:10:09 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "sha2.h"

void					ssl_sha2_384_init(t_sha2_ctx *ctx)
{
	*ctx = (t_sha2_ctx){
		.insert_pad = true,
		.last_block = true,
		.md = (t_sha2_hash){
			.lstate = {
				0xcbbb9d5dc1059ed8,
				0x629a292a367cd507,
				0x9159015a3070dd17,
				0x152fecd8f70e5939,
				0x67332667ffc00b31,
				0x8eb44a8768581511,
				0xdb0c2e0d64f98fa7,
				0x47b5481dbefa4fa4,
			},
		},
	};
}
