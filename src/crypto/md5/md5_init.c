/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_init.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/08 16:15:47 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/11 09:04:12 by mbeilles         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "digest/md5.h"

void					ssl_md5_init(t_md5_ctx *ctx)
{
	*ctx = (t_md5_ctx){
		.insert_pad = true,
		.last_block = true,
		.md = (t_md5_hash){
				.h0 = 0x67452301,
				.h1 = 0xefcdab89,
				.h2 = 0x98badcfe,
				.h3 = 0x10325476
		},
	};
}
