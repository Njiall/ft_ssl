/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_final_bin.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/09 10:19:39 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/17 06:48:59 by njiall           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "md5.h"
#include "libft.h"

t_ssl_digest			ssl_md5_final_bin(t_md5_ctx *ctx)
{
	static uint8_t		str[128 / 8];

	ft_memcpy(str, ctx->md.hash, sizeof(ctx->md.hash));
	return ((t_ssl_digest){.hash = str, .length = 128 / 8});
}
