/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5_update.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/08 16:16:20 by mbeilles          #+#    #+#             */
/*   Updated: 2019/12/21 12:51:38 by njiall           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "md5.h"
#include "libft.h"

bool					ssl_md5_update_data(
		t_md5_ctx *ctx,
		const uint8_t *buff,
		ssize_t len
)
{
	ft_memcpy(ctx->data, buff, len);
	ft_memset(ctx->data + len, 0, sizeof(ctx->data) - len);
	if (len <= 0 && !ctx->last_block)
		return (false);
	ctx->data_len += len;
	if (len != sizeof(ctx->data) && ctx->insert_pad)
	{
		ctx->data[len] = 0b10000000;
		ctx->insert_pad = false;
	}
	if (len != sizeof(ctx->data)
			&& len <= sizeof(ctx->data) - sizeof(ctx->data_len) - 1
			&& ctx->last_block)
	{
		ctx->bit_len = ctx->data_len * 8;
		ft_memcpy(ctx->data + 64 - sizeof(ctx->data_len), &ctx->bit_len,
				sizeof(ctx->data_len));
		// puts len as little endian in data
		ctx->last_block = false;
	}
	return (true);
}
