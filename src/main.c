/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbeilles <mbeilles@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/28 14:21:30 by mbeilles          #+#    #+#             */
/*   Updated: 2020/05/22 18:13:02 by njiall           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include "command.h"
#include "libft.h"

static const int		g_exit_codes[SSL_ERR_MAX] = {
	[SSL_ERR_INPUT_START ... SSL_ERR_INPUT_END] = 1,
	[SSL_ERR_INTERNAL_START ... SSL_ERR_INTERNAL_END] = 1,
	[SSL_ERR_CRASH_START ... SSL_ERR_CRASH_END] = 2,
};

int						main(const int argc, const char **argv)
{
	t_ssl_cmd_status	status;
	if (!(status = ssl_command_handle(argc, argv)).valid)
		ssl_command_status_print(status);
	return (g_exit_codes[status.code]);
}
