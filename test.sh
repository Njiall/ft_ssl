#!/bin/bash
passed=0
failed=0

function print_tests() {
	printf "Tested: \e[1;32m$passed\e[0;2m/\e[0;1;31m$failed\e[0;2m/\e[0;1m$(($passed + $failed))\e[0m\n"
	exit
}

function print_load() {
	local w=$(( $(tput cols) - 9 )) p=$1; m=$2; shift 2
	printf -v bar "%*s" "$(( $p * $w / $m ))" ""; bar=${bar// /-};
	printf "\r\e[K\e[2m|\e[0;1;33m%-*s\e[0;2m|\e[0m %3d %% %s" "$w" "$bar" "$(( $p * 100 / $m ))"
}

trap print_tests SIGINT

mkdir -p failed/

nbr=$1
shift
cmds="$*"
for cmd in $cmds; do
	printf "Testing: '$cmd'\n"
	for i in $(seq 1 $nbr); do
		print_load $i $nbr
		head -10 /dev/urandom > test
		out1="$(./ft_ssl $cmd test)"
		out2="$(openssl $cmd test)"

		if [[ "${out1}" != "$out2" ]]; then
			failed=$(( $failed + 1 ))
			printf "diff:\n+$out1\n-$out2\n" | cat -e
			cp test "failed/test$failed"
		else
			passed=$(( $passed + 1 ))
		fi
	done
	printf "\n"
done

print_tests
